package com.odigeo.product.membership.service;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipFee;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MembershipProductService;
import com.odigeo.membership.member.UpdateMembershipService;
import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;
import com.odigeo.util.FeeMapper;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class ProductServiceV2Test {

    private static final String PRODUCT_ID = "123";
    private static final String WEBSITE = "ES";
    private static final String TRANSACTION_PRODUCT_ID = "123";
    private static final String CURRENCY = "EUR";
    private static final String MEMBERSHIP_ID = "321";
    private static final Long MEMBER_ACCOUNT_ID = 111L;
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final LocalDateTime NEXT_YEAR = NOW.plusYears(1);
    private static final String RENEWAL_FEE_SUBCODE = "AE10";
    private static final String DEFAULT_EXCEPTION = "Default Exception";
    private static final DataAccessException DATA_ACCESS_EXCEPTION = new DataAccessException(DEFAULT_EXCEPTION);
    private static final MissingElementException MISSING_ELEMENT_EXCEPTION = new MissingElementException(DEFAULT_EXCEPTION);

    private static final Membership MEMBERSHIP = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.INIT).build();
    private static final Membership MEMBERSHIP_CONTRACT = new MembershipBuilder().setId(Long.parseLong(MEMBERSHIP_ID)).setWebsite(WEBSITE).setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW).setExpirationDate(NEXT_YEAR).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
    private static final MembershipFee MEMBERSHIP_FEE = new MembershipFee(MEMBERSHIP_ID, BigDecimal.TEN, CURRENCY, "RENEWAL_FEE");

    private ProductServiceV2 productService;

    @Mock
    private Appender mockAppender;
    @Mock
    private MemberService memberService;
    @Mock
    private UpdateMembershipService updateMembershipService;
    @Mock
    private MembershipProductService membershipProductService;
    @Mock
    private MembershipFeesService membershipFeesService;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        productService = new ProductServiceV2New(memberService, membershipProductService, updateMembershipService, membershipFeesService);
    }

    @AfterMethod
    public void tearDownLogger() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testGetProductHappyPathWithSubCode() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyLong())).thenReturn(MEMBERSHIP);
        when(membershipProductService.getMembershipFees(anyLong())).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(membershipFeesService.fillProductFee(any(), any())).thenAnswer(invocation -> {
            Fee fee = new Fee();
            fee.setSubCode(FeeMapper.MEMBERSHIP_RENEWAL_FEE_SUBCODE);
            return fee;
        });
        verifyProductFees();
    }

    private void verifyProductFees() throws ProductException {
        final Product product = productService.getProduct(PRODUCT_ID);
        assertNotNull(product);
        final List<Fee> fees = product.getFees();
        assertNotNull(fees);
        assertFalse(fees.isEmpty());
        final Fee fee = fees.get(0);
        assertNotNull(fee);
        assertEquals(fee.getSubCode(), RENEWAL_FEE_SUBCODE);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductWhenNotFound() throws ProductException {
        productService.getProduct(null);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductDataAccessException() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyLong())).thenThrow(DATA_ACCESS_EXCEPTION);
        productService.getProduct(PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetProductMissingElementException() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyLong())).thenThrow(MISSING_ELEMENT_EXCEPTION);
        productService.getProduct(PRODUCT_ID);
    }

    @Test
    public void testGetBookingApiProductHappyPath() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyLong())).thenReturn(MEMBERSHIP);
        when(membershipProductService.getMembershipFees(anyLong())).thenReturn(Collections.singletonList(MEMBERSHIP_FEE));
        when(membershipFeesService.fillProductFee(any(), any())).thenAnswer(ProductServiceV2Test::mockFee);
        assertNotNull(productService.getBookingApiProductInfo(PRODUCT_ID));
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetBookingApiProductWhenNotFound() throws ProductException {
        productService.getBookingApiProductInfo(null);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetBookingApiProductMissingElementException() throws ProductException, MissingElementException, DataAccessException {
        setBookingApiProductThrowException(MISSING_ELEMENT_EXCEPTION);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testGetBookingApiProductDataAccessException() throws ProductException, MissingElementException, DataAccessException {
        setBookingApiProductThrowException(DATA_ACCESS_EXCEPTION);
    }

    private void setBookingApiProductThrowException(Throwable exception) throws MissingElementException, DataAccessException, ProductException {
        when(memberService.getMembershipById(anyLong())).thenThrow(exception);
        productService.getBookingApiProductInfo(PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testSaveCompleteOrderNotInContract() throws ProductException, DataAccessException, MissingElementException {
        setUpMembership();
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
    }

    @Test
    public void testSaveCompleteOrderInContract() throws ProductException, DataAccessException, MissingElementException {
        setUpMembershipWithStatusContract();
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, false);
        verify(memberService).getMembershipById(Long.parseLong(PRODUCT_ID));
    }

    @Test
    public void testSavePartialOrderHappyPath() throws ProductException, MissingElementException, DataAccessException {
        productService.saveProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, true);
        verify(memberService, never()).getMembershipById(Long.parseLong(PRODUCT_ID));
    }

    @Test
    public void testCommitTransactionOrderHappyPath() throws ProductException, MissingElementException, DataAccessException {
        setUpMembership();
        productService.commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        verify(updateMembershipService).activateRenewalPendingToCollect(any(Membership.class));
    }

    @Test(expectedExceptions = ProductException.class)
    public void commitTransactionProductStatusNotInit() throws ProductException, MissingElementException, DataAccessException {
        setUpMembershipWithStatusContract();
        productService.commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void testCommitTransactionMembershipNotFound() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenThrow(MISSING_ELEMENT_EXCEPTION);
        productService.commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void commitTransactionProduct() throws ProductException, MissingElementException, DataAccessException {
        when(memberService.getMembershipById(Long.parseLong(PRODUCT_ID))).thenThrow(DATA_ACCESS_EXCEPTION);
        productService.commitTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
    }

    @Test(expectedExceptions = ProductException.class)
    public void commitTransactionProductWhenNotFound() throws ProductException {
        productService.commitTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    private void setUpMembership() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyInt())).thenReturn(MEMBERSHIP);
    }

    private void setUpMembershipWithStatusContract() throws MissingElementException, DataAccessException {
        when(memberService.getMembershipById(anyInt())).thenReturn(MEMBERSHIP_CONTRACT);
    }

    @Test(expectedExceptions = ProductException.class)
    public void getCloseTransactionProductResultWhenNotFound() throws ProductException {
        productService.saveProduct(null, TRANSACTION_PRODUCT_ID, false);
    }

    @Test
    public void rollbackTransactionProduct() throws ProductException {
        productService.rollbackTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        verifyLoggedMessage(ProductServiceV2.ROLLBACK_TRANSACTION_PRODUCT);
    }

    private void verifyLoggedMessage(String methodName) {
        verify(mockAppender, atLeastOnce()).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        String logMessage = "Call function: " + methodName
                + " with ProductId: " + PRODUCT_ID
                + " and TransactionProductId " + TRANSACTION_PRODUCT_ID;
        assertTrue(loggingEvents.stream()
                .anyMatch(loggingEvent -> loggingEvent.getRenderedMessage().contains(logMessage)));
    }

    @Test(expectedExceptions = ProductException.class)
    public void rollbackTransactionProductWhenNotFound() throws ProductException {
        productService.rollbackTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void cancelTransactionProduct() throws ProductException {
        productService.cancelTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        verifyLoggedMessage(ProductServiceV2.CANCEL_TRANSACTION_PRODUCT);
    }

    @Test(expectedExceptions = ProductException.class)
    public void cancelTransactionProductWhenNotFound() throws ProductException {
        productService.cancelTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void suspendTransactionProduct() throws ProductException {
        productService.suspendTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        verifyLoggedMessage(ProductServiceV2.SUSPEND_TRANSACTION_PRODUCT);
    }

    @Test(expectedExceptions = ProductException.class)
    public void suspendTransactionProductWhenNotFound() throws ProductException {
        productService.suspendTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void resumeTransactionProduct() throws ProductException {
        productService.resumeTransactionProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID);
        verifyLoggedMessage(ProductServiceV2.RESUME_TRANSACTION_PRODUCT);
    }

    @Test(expectedExceptions = ProductException.class)
    public void resumeTransactionProductWhenNotFound() throws ProductException {
        productService.resumeTransactionProduct(null, TRANSACTION_PRODUCT_ID);
    }

    @Test
    public void closeProviderProduct() {
        CloseProviderProductResponse closeProviderProductResponse = productService.closeProviderProduct(PRODUCT_ID, TRANSACTION_PRODUCT_ID, null);
        assertEquals(closeProviderProductResponse.getCloseActionResult(), CloseActionResult.OK);
        assertEquals(closeProviderProductResponse.getProviderProductStatus(), ProviderProductStatus.CONTRACT);
    }

    private static Fee mockFee(InvocationOnMock invocation) {
        Fee fee = new Fee();
        Price price = new Price();
        price.setAmount(BigDecimal.TEN);
        price.setCurrency(Currency.getInstance(CURRENCY));
        fee.setPrice(price);
        return fee;
    }
}

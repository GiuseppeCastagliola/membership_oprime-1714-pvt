package com.odigeo.membership;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class MemberStatusAction implements Serializable {

    private final Long id;
    private final Long memberId;
    private final StatusAction action;
    private final Date timestamp;

    public MemberStatusAction(Long id, Long memberId, StatusAction action, Date timestamp) {
        this.id = id;
        this.memberId = memberId;
        this.action = action;
        this.timestamp = timestamp != null ? new Date(timestamp.getTime()) : null;
    }

    public Long getId() {
        return id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public StatusAction getAction() {
        return action;
    }

    public Date getTimestamp() {
        return timestamp == null ? null : new Date(timestamp.getTime());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MemberStatusAction that = (MemberStatusAction) o;
        return Objects.equals(id, that.id)
                && Objects.equals(memberId, that.memberId)
                && action == that.action
                && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, memberId, action, timestamp);
    }

    @Override
    public String toString() {
        return "MemberStatusAction{" + "id=" + id + ", memberId=" + memberId + ", action=" + action + ", timestamp=" + timestamp + '}';
    }
}

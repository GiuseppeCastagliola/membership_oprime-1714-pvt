package com.odigeo.membership;

import java.util.Objects;

public class PostBookingPageInfo {
    public static final String EXCEPTION_MESSAGE = "Something went wrong with the given token";
    private long bookingId;
    private String email;

    public PostBookingPageInfo() {
    }

    public PostBookingPageInfo(long bookingId, String email) {
        this.bookingId = bookingId;
        this.email = email;
    }

    public long getBookingId() {
        return bookingId;
    }

    public PostBookingPageInfo setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PostBookingPageInfo setEmail(String email) {
        this.email = email;
        return this;
    }

    public void validatePageInfo() {
        if (this.email == null || this.email.isEmpty() || this.bookingId == 0) {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PostBookingPageInfo that = (PostBookingPageInfo) o;
        return bookingId == that.bookingId
                && Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, email);
    }
}

package com.odigeo.membership.parameters;

import java.util.List;

public class MemberOnPassengerListParameter {
    private Long userId;
    private String site;
    private List<TravellerParameter> travellerList;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public List<TravellerParameter> getTravellerList() {
        return travellerList;
    }

    public void setTravellerList(List<TravellerParameter> travellerList) {
        this.travellerList = travellerList;
    }
}

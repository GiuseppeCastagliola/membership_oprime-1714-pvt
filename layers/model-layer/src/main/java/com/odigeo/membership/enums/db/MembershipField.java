package com.odigeo.membership.enums.db;

import java.util.Arrays;

import static com.odigeo.membership.enums.db.FieldName.TableAlias.MEMBERSHIP_ALIAS;
import static java.util.stream.Collectors.joining;

public enum MembershipField implements FieldName {
    ID, WEBSITE, TIMESTAMP, STATUS, AUTO_RENEWAL, MEMBER_ACCOUNT_ID, EXPIRATION_DATE, USER_CREDIT_CARD_ID,
    ACTIVATION_DATE, MEMBERSHIP_TYPE, BALANCE, MONTHS_DURATION, PRODUCT_STATUS, TOTAL_PRICE, RENEWAL_PRICE,
    MONTHS_RENEWAL_DURATION, CURRENCY_CODE, SOURCE_TYPE, FEE_CONTAINER_ID;

    public static final String MEMBERSHIP_FIELDS = Arrays.stream(MembershipField.values())
            .map(MembershipField::withTableAlias)
            .collect(joining(", "));

    public String withTableAlias() {
        String name = name();
        StringBuilder fieldNameBuilder = new StringBuilder(commonName());
        if (TIMESTAMP.name().equals(name)) {
            fieldNameBuilder.append(" as MEMBERSHIP_TIMESTAMP");
        }
        return fieldNameBuilder.toString();
    }

    public String filteringName() {
        return commonName();
    }

    private String commonName() {
        return MEMBERSHIP_ALIAS.toString() + "." + name();
    }


}

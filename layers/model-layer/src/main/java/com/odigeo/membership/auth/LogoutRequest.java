package com.odigeo.membership.auth;

import java.util.Objects;

public class LogoutRequest extends AuthServiceRequest {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LogoutRequest logoutRequest = (LogoutRequest) o;
        return Objects.equals(getToken(), logoutRequest.getToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getToken());
    }
}

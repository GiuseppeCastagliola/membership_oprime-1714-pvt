package com.odigeo.membership.auth;

import java.util.Objects;

public class AuthorizationRequest extends AuthServiceRequest {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorizationRequest authorizationRequest = (AuthorizationRequest) o;
        return Objects.equals(getToken(), authorizationRequest.getToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getToken());
    }
}

package com.odigeo.membership;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MemberStatusTest {

    @Test
    public void testEnum() {
        assertNotNull(MemberStatus.valueOf("PENDING_TO_ACTIVATE"));
        assertNotNull(MemberStatus.valueOf("ACTIVATED"));
        assertNotNull(MemberStatus.valueOf("EXPIRED"));
        assertNotNull(MemberStatus.valueOf("DISCARDED"));
    }

    @Test
    public void testIsStatusTransitionAllowed() {
        assertTrue(MemberStatus.ACTIVATED.isStatusTransitionAllowed(MemberStatus.DEACTIVATED));
        assertTrue(MemberStatus.ACTIVATED.isStatusTransitionAllowed(MemberStatus.EXPIRED));
        assertFalse(MemberStatus.ACTIVATED.isStatusTransitionAllowed(MemberStatus.DISCARDED));
    }

    @Test
    public void testIs() {
        assertFalse(MemberStatus.PENDING_TO_ACTIVATE.is(MemberStatus.ACTIVATED));
        assertFalse(MemberStatus.PENDING_TO_COLLECT.is(MemberStatus.PENDING_TO_ACTIVATE));
        assertTrue(MemberStatus.ACTIVATED.is(MemberStatus.ACTIVATED));
        assertTrue(MemberStatus.DEACTIVATED.is(MemberStatus.DEACTIVATED));
    }

    @Test
    public void testIsNot() {
        assertTrue(MemberStatus.PENDING_TO_ACTIVATE.isNot(MemberStatus.ACTIVATED));
        assertTrue(MemberStatus.PENDING_TO_COLLECT.isNot(MemberStatus.PENDING_TO_ACTIVATE));
        assertFalse(MemberStatus.ACTIVATED.isNot(MemberStatus.ACTIVATED));
        assertFalse(MemberStatus.DEACTIVATED.isNot(MemberStatus.DEACTIVATED));
    }
}

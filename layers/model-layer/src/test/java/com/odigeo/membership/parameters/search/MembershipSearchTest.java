package com.odigeo.membership.parameters.search;

import bean.test.BeanTest;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipSearchTest extends BeanTest<MembershipSearch> {

    private static final Long USER_ID = 123L;
    private static final String CURRENCY = "EUR";
    private static final String TEST_DATE = "2021-12-12";
    private static final String WEBSITE = "ES";
    private static final String LAST_NAME = "Sanchez";
    private static final String NAME = "Mario";
    private static final int DURATION = 12;

    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE = " SELECT gmr.RECURRING_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, " +
            "m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE," +
            " m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID FROM  GE_MEMBERSHIP m " +
            " LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  WHERE m.WEBSITE = ? ";
    private static final String SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID = " SELECT gmr.RECURRING_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, " +
            "m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE, m.MEMBERSHIP_TYPE, m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, " +
            "a.ID as ACCOUNT_ID, a.USER_ID, a.FIRST_NAME, a.LAST_NAME, a.TIMESTAMP FROM  GE_MEMBERSHIP m  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  JOIN  GE_MEMBER_ACCOUNT a ON a.ID = m.MEMBER_ACCOUNT_ID  WHERE m.WEBSITE = ?  AND a.USER_ID = ? ";

    @Test
    public void testEmptiness() {
        MembershipSearch membershipSearch = new MembershipSearchBuilder().build();
        assertTrue(membershipSearch.getValues().isEmpty());
        assertTrue(membershipSearch.isEmpty());
        MembershipSearch membershipWebsiteSearch = new MembershipSearchBuilder().website(WEBSITE).build();
        assertFalse(membershipWebsiteSearch.getValues().isEmpty());
        assertFalse(membershipWebsiteSearch.isEmpty());
    }

    @Test
    public void testGetQueryString() {
        MembershipSearchBuilder builder = new MembershipSearchBuilder();
        MembershipSearch membershipWebsiteSearch = builder.website(WEBSITE).build();
        assertEquals(membershipWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE);
        MembershipSearch membershipUserIdAndWebsiteSearch = builder.withMemberAccount(true)
                .memberAccountSearch(new MemberAccountSearch.Builder().userId(USER_ID).build())
                .build();
        assertEquals(membershipUserIdAndWebsiteSearch.getQueryString(), SEARCH_MEMBERSHIP_BY_WEBSITE_AND_USER_ID);
    }

    @Override
    protected MembershipSearch getBean() {
        return new MembershipSearchBuilder()
                .autoRenewal(MembershipRenewal.DISABLED.name())
                .currencyCode(CURRENCY)
                .fromActivationDate(TEST_DATE)
                .toActivationDate(TEST_DATE)
                .fromExpirationDate(TEST_DATE)
                .toExpirationDate(TEST_DATE)
                .maxBalance(BigDecimal.TEN)
                .membershipType(MembershipType.BASIC.name())
                .minBalance(BigDecimal.ONE)
                .monthsDuration(1)
                .productStatus(ProductStatus.INIT.name())
                .sourceType(SourceType.FUNNEL_BOOKING.name())
                .status(MemberStatus.ACTIVATED.name())
                .totalPrice(BigDecimal.TEN)
                .renewalPrice(BigDecimal.ONE)
                .renewalDuration(DURATION)
                .website(WEBSITE)
                .withMemberAccount(true)
                .withStatusActions(true)
                .memberAccountSearch(new MemberAccountSearch.Builder()
                        .lastNames(LAST_NAME)
                        .userId(USER_ID)
                        .name(NAME).build())
                .build();
    }
}

package com.odigeo.membership;

import bean.test.BeanTest;
import com.odigeo.bookingapi.v14.responses.Money;

public class PrimeBookingInformationTest extends BeanTest<PrimeBookingInformation> {

    private static final long BOOKING_ID_ONE = 1;

    @Override
    protected PrimeBookingInformation getBean() {
        return new PrimeBookingInformation()
                .setBookingId(BOOKING_ID_ONE)
                .setDiscount(new Money())
                .setPrice(new Money());
    }
}

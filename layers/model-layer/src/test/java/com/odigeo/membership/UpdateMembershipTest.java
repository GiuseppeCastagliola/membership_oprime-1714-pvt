package com.odigeo.membership;

import bean.test.BeanTest;

public class UpdateMembershipTest extends BeanTest<UpdateMembership> {

    @Override
    protected UpdateMembership getBean() {
        return new UpdateMembership();
    }
}
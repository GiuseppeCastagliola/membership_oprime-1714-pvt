package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class AuthenticationResponseTest extends BeanTest<AuthenticationResponse> {
    @Test
    public void authenticationResponseEqualsVerifierTest() {
        EqualsVerifier.forClass(AuthenticationResponse.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected AuthenticationResponse getBean() {
        return assembleAuthenticationResponse();
    }

    private AuthenticationResponse assembleAuthenticationResponse() {
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setEmail("test@test.com");
        authenticationResponse.setName("name");
        authenticationResponse.setToken("token");
        authenticationResponse.setUserId("user");
        return authenticationResponse;
    }
}
package com.odigeo.membership.parameters.search;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MemberAccountSearchTest extends BeanTest<MemberAccountSearch> {

    private static final String LAST_NAMES = "Sanchez";
    private static final String FIRST_NAME = "Mario";
    private static final Long USER_ID = 123L;

    private static final String VOID_SEARCH_QUERY = " SELECT a.ID as ACCOUNT_ID, a.USER_ID, a.FIRST_NAME, a.LAST_NAME, a.TIMESTAMP FROM  GE_MEMBER_ACCOUNT a ";
    private static final String LAST_NAME_SEARCH_QUERY = VOID_SEARCH_QUERY + " WHERE a.LAST_NAME = ? ";
    private static final String FULL_NAME_SEARCH_QUERY = VOID_SEARCH_QUERY + " WHERE a.FIRST_NAME = ?  AND a.LAST_NAME = ? ";
    private static final String FULL_SEARCH_QUERY = VOID_SEARCH_QUERY + " WHERE a.USER_ID = ?  AND a.FIRST_NAME = ?  AND a.LAST_NAME = ? ";
    private static final String FULL_SEARCH_QUERY_WITH_MEMBERSHIP = " SELECT gmr.RECURRING_ID, m.ID, m.WEBSITE, m.TIMESTAMP as MEMBERSHIP_TIMESTAMP, m.STATUS, m.AUTO_RENEWAL, m.MEMBER_ACCOUNT_ID, m.EXPIRATION_DATE, m.USER_CREDIT_CARD_ID, m.ACTIVATION_DATE," +
            " m.MEMBERSHIP_TYPE, m.BALANCE, m.MONTHS_DURATION, m.PRODUCT_STATUS, m.TOTAL_PRICE, m.RENEWAL_PRICE, m.MONTHS_RENEWAL_DURATION, m.CURRENCY_CODE, m.SOURCE_TYPE, m.FEE_CONTAINER_ID, a.ID as ACCOUNT_ID, a.USER_ID, a.FIRST_NAME, a.LAST_NAME, a.TIMESTAMP FROM  GE_MEMBERSHIP m" +
            "  LEFT OUTER JOIN GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID  JOIN  GE_MEMBER_ACCOUNT a ON a.ID = m.MEMBER_ACCOUNT_ID  WHERE a.USER_ID = ?  AND a.FIRST_NAME = ?  AND a.LAST_NAME = ? ORDER BY m.EXPIRATION_DATE DESC";

    MemberAccountSearch.Builder baseBuilder = new MemberAccountSearch.Builder();
    MemberAccountSearch lastNameSearch = baseBuilder.lastNames(LAST_NAMES).build();
    MemberAccountSearch fullNameSearch = baseBuilder.name(FIRST_NAME).build();
    MemberAccountSearch fullSearch = baseBuilder.userId(USER_ID).build();
    MemberAccountSearch fullSearchWithMembership = baseBuilder.withMembership(true).build();

    @Test
    public void testEmptiness() {
        MemberAccountSearch memberAccountSearch = new MemberAccountSearch.Builder().build();
        assertTrue(memberAccountSearch.getValues().isEmpty());
        assertTrue(memberAccountSearch.isEmpty());
        assertFalse(lastNameSearch.getValues().isEmpty());
        assertFalse(lastNameSearch.isEmpty());
    }

    @Test
    public void testGetValues() {
        checkValues(lastNameSearch, LAST_NAMES);
        checkValues(fullNameSearch, LAST_NAMES, FIRST_NAME);
        checkValues(fullSearch, LAST_NAMES, FIRST_NAME, USER_ID);
    }

    private void checkValues(MemberAccountSearch memberAccountSearch, Object... searchParams) {
        List<Object> searchValues = memberAccountSearch.getValues();
        assertFalse(searchValues.isEmpty());
        assertTrue(searchValues.containsAll(Arrays.asList(searchParams)));
    }

    @Test
    public void testGetQueryString() {
        assertEquals(lastNameSearch.getQueryString(), LAST_NAME_SEARCH_QUERY);
        assertEquals(fullNameSearch.getQueryString(), FULL_NAME_SEARCH_QUERY);
        assertEquals(fullSearch.getQueryString(), FULL_SEARCH_QUERY);
        assertEquals(fullSearchWithMembership.getQueryString(), FULL_SEARCH_QUERY_WITH_MEMBERSHIP);
    }

    @Test
    public void testIsWithMemberships() {
        assertFalse(lastNameSearch.isWithMemberships() || fullNameSearch.isWithMemberships() || fullSearch.isWithMemberships());
        assertTrue(fullSearchWithMembership.isWithMemberships());
    }

    @Override
    protected MemberAccountSearch getBean() {
        return new MemberAccountSearch.Builder()
                .lastNames(LAST_NAMES)
                .name(FIRST_NAME)
                .userId(USER_ID)
                .withMembership(true)
                .build();
    }
}

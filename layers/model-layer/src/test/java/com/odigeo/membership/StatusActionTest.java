package com.odigeo.membership;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StatusActionTest {

    @Test
    public void testEnum() throws Exception {
        Assert.assertNotNull(StatusAction.valueOf("CREATION"));
        Assert.assertNotNull(StatusAction.valueOf("ACTIVATION"));
        Assert.assertNotNull(StatusAction.valueOf("EXPIRATION"));
        Assert.assertNotNull(StatusAction.valueOf("RENOVATION"));
        Assert.assertNotNull(StatusAction.valueOf("DISCARD"));
    }

}
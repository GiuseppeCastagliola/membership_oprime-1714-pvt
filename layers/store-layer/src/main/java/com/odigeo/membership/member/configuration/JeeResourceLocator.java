package com.odigeo.membership.member.configuration;

import com.odigeo.membership.exception.UnavailableDataSourceException;
import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public final class JeeResourceLocator implements ResourceLocator {

    public static final String DEFAULT_DATASOURCE_NAME = "java:/jdbc/oracle_membership_default_rw";

    private static final Logger logger = Logger.getLogger(JeeResourceLocator.class);
    private static final JeeResourceLocator THE_INSTANCE = new JeeResourceLocator();

    private InitialContext jndiContext;

    private JeeResourceLocator() {
        try {
            initContext();
        } catch (NamingException nex) {
            logger.error("Unable to find initial JNDI context.", nex);
        }
    }

    public static JeeResourceLocator getInstance() {
        return THE_INSTANCE;
    }

    private void initContext() throws NamingException {
        if (jndiContext == null) {
            jndiContext = new InitialContext();
        }
    }

    private DataSource getDataSource(final String name) throws NamingException {
        initContext();
        return (DataSource) jndiContext.lookup(name);
    }

    @Override
    public DataSource getDefaultDataSource() throws UnavailableDataSourceException {
        try {
            return getDataSource(DEFAULT_DATASOURCE_NAME);
        } catch (NamingException nex) {
            throw new UnavailableDataSourceException(DEFAULT_DATASOURCE_NAME, nex);
        }

    }

}

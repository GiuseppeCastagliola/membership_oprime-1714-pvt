package com.odigeo.membership.redis;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import static java.util.Objects.isNull;

@Singleton
public class RedisClient {

    private static final int CONCURRENT_CONNECTIONS = 128;

    private final RedisClientConfiguration redisClientConfiguration;
    private JedisPool jedisPool;

    @Inject
    public RedisClient(RedisClientConfiguration redisClientConfiguration) {
        this.redisClientConfiguration = redisClientConfiguration;
    }

    @VisibleForTesting
    void setJedisPool(JedisPool jedisPool) {
        if (isNull(this.jedisPool)) {
            this.jedisPool = jedisPool;
        }
    }

    private JedisPoolConfig configureJedisPool() {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(CONCURRENT_CONNECTIONS);
        return poolConfig;
    }

    public Jedis getJedisConnection() {
        if (isNull(jedisPool)) {
            jedisPool = new JedisPool(configureJedisPool(), redisClientConfiguration.getHost(),
                    redisClientConfiguration.getPort());
        }
        return jedisPool.getResource();
    }

    public String getRedisClientConfiguration() {
        return redisClientConfiguration.getHost() + ":" + redisClientConfiguration.getPort();
    }
}

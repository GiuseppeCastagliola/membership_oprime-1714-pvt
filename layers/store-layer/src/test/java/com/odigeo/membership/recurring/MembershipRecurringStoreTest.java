package com.odigeo.membership.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.DataNotFoundException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MembershipRecurringStoreTest {

    private static final String RECURRING_ID = "ABC";
    private static final Long RECURRING_ID_INDEX = 1L;

    private MembershipRecurringStore store;

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockDatabase();
        ConfigurationEngine.init();
        store = ConfigurationEngine.getInstance(MembershipRecurringStore.class);
    }

    @Test
    public void testGetMembershipRecurringOK() throws Exception {
        resultSetWithOneResult();
        resultSetValue(RECURRING_ID);
        assertEquals(store.getMembershipRecurring(dataSource, 1L),RECURRING_ID);
        verify(preparedStatement, times(1)).setString(anyInt(), anyString());
        verify(resultSet, times(1)).getString(anyString());
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testGetMembershipRecurringKO() throws Exception {
        resultSetWithNoResults();
        store.getMembershipRecurring(dataSource, 1L);
    }

    @Test
    public void testInsertRecurring() throws Exception {
        resultSetWithOneResult();
        when(preparedStatement.execute()).thenReturn(true);
        when(resultSet.getLong(1)).thenReturn(RECURRING_ID_INDEX);
        store.insertMembershipRecurring(dataSource, 1L, RECURRING_ID);
        verify(preparedStatement, times(1)).setNull(anyInt(), anyInt());
        verify(preparedStatement, times(2)).setString(anyInt(), anyString());
    }

    private void resultSetValue(String recurringId) throws SQLException {
        when(resultSet.getString(any())).thenReturn(recurringId);
    }

    private void resultSetWithOneResult() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    }

    private void resultSetWithNoResults() throws SQLException {
        when(resultSet.next()).thenReturn(false);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
    }

}

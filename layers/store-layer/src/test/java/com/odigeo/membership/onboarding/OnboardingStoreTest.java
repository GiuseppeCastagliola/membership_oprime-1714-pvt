package com.odigeo.membership.onboarding;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;


public class OnboardingStoreTest {

    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    private OnboardingStore store;

    @BeforeMethod
    public void setUp() throws SQLException {
        initMocks(this);
        mockDatabase();
        store = new OnboardingStore();
    }

    private void mockDatabase() throws SQLException {
        given(dataSource.getConnection()).willReturn(connection);
        given(connection.prepareStatement(anyString())).willReturn(preparedStatement);
        given(preparedStatement.executeQuery()).willReturn(resultSet);
    }

    @Test
    public void testCreateOnboarding() throws SQLException {
        //Given
        long generatedOnboardingId = 324L;
        long memberAccountId = 123L;
        OnboardingEvent event = OnboardingEvent.COMPLETED;
        OnboardingDevice device = OnboardingDevice.APP;
        given(resultSet.next()).willReturn(true);
        given(resultSet.getLong(1)).willReturn(generatedOnboardingId);

        //When
        long createdOnboardingId = store.createOnboarding(dataSource, memberAccountId, event, device);

        //Then
        verify(preparedStatement).setLong(1, generatedOnboardingId);
        verify(preparedStatement).setLong(2, memberAccountId);
        verify(preparedStatement).setString(3, "COMPLETED");
        verify(preparedStatement).setString(4, "APP");
        assertEquals(createdOnboardingId, generatedOnboardingId);
    }
}
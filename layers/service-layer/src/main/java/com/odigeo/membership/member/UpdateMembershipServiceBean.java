package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.renewal.MembershipRenewalService;
import com.odigeo.membership.member.update.MembershipUpdateOperation;
import com.odigeo.membership.tracking.autorenewal.MembershipAutorenewalTracker;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.EnumMap;
import java.util.Optional;

@Stateless
@Local(UpdateMembershipService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UpdateMembershipServiceBean extends AbstractServiceBean implements UpdateMembershipService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateMembershipServiceBean.class);

    private final EnumMap<UpdateMembershipAction, MembershipUpdateOperation> updateOperationMap = new EnumMap<>(UpdateMembershipAction.class);
    private final EnumMap<MemberStatus, MembershipRenewal> transitionChangeStatusToAutoRenewal = new EnumMap<>(MemberStatus.class);

    private final MembershipUpdateOperation deactivateMemberOperation = updateMembership -> MemberStatus.DEACTIVATED.name().equals(deactivateMembership(updateMembership));
    private final MembershipUpdateOperation reactivateMemberOperation = updateMembership -> MemberStatus.ACTIVATED.name().equals(reactivateMembership(updateMembership));
    private final MembershipUpdateOperation enableAutoRenewalOperation = updateMembership -> MembershipRenewal.ENABLED.name().equals(enableAutoRenewal(updateMembership));
    private final MembershipUpdateOperation disableAutoRenewalOperation = updateMembership -> MembershipRenewal.DISABLED.name().equals(disableAutoRenewal(updateMembership));
    private final MembershipUpdateOperation expireMemberOperation = updateMembership -> MemberStatus.EXPIRED.name().equals(expireMembership(updateMembership));
    private final MembershipUpdateOperation discardMembership = updateMembership -> MemberStatus.DISCARDED.name().equals(discardMembership(updateMembership));
    private final MembershipUpdateOperation consumeMembershipRemnantBalance = this::consumeMembershipRemnantBalance;
    private final MembershipUpdateOperation addRecurringId = this::addRecurringId;

    @EJB
    private MemberService memberService;
    private MembershipRenewalService membershipRenewalService;

    public UpdateMembershipServiceBean() {
        updateOperationMap.put(UpdateMembershipAction.DEACTIVATE_MEMBERSHIP, deactivateMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.REACTIVATE_MEMBERSHIP, reactivateMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.ENABLE_AUTO_RENEWAL, enableAutoRenewalOperation);
        updateOperationMap.put(UpdateMembershipAction.DISABLE_AUTO_RENEWAL, disableAutoRenewalOperation);
        updateOperationMap.put(UpdateMembershipAction.EXPIRE_MEMBERSHIP, expireMemberOperation);
        updateOperationMap.put(UpdateMembershipAction.CONSUME_MEMBERSHIP_REMNANT_BALANCE, consumeMembershipRemnantBalance);
        updateOperationMap.put(UpdateMembershipAction.DISCARD_MEMBERSHIP, discardMembership);
        updateOperationMap.put(UpdateMembershipAction.INSERT_RECURRING_ID, addRecurringId);
        transitionChangeStatusToAutoRenewal.put(MemberStatus.ACTIVATED, MembershipRenewal.ENABLED);
        transitionChangeStatusToAutoRenewal.put(MemberStatus.DEACTIVATED, MembershipRenewal.DISABLED);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public Boolean updateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException, BookingApiException, ExistingRecurringException {
        return Optional.ofNullable(updateOperationMap.get(UpdateMembershipAction.valueOf(updateMembership.getOperation())))
                .orElseThrow(() -> new InvalidParameterException("Unknown operation parameter: " + updateMembership.getOperation()))
                .apply(updateMembership);
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String disableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.DISABLED, MembershipRenewal.ENABLED, membershipId -> getMemberManager().disableAutoRenewal(dataSource, membershipId));
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String enableAutoRenewal(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        return changeAutoRenewal(Long.parseLong(updateMembership.getMembershipId()), MembershipRenewal.ENABLED, MembershipRenewal.DISABLED, membershipId -> getMemberManager().enableAutoRenewal(dataSource, membershipId));
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String deactivateMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.ACTIVATED) {
            return changeStatusMembership(membership, MemberStatus.DEACTIVATED, StatusAction.DEACTIVATION);
        }
        return membership.getStatus().toString();
    }

    @Override
    @Interceptors({MembershipAutorenewalTracker.class})
    public String reactivateMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        if (membership.getStatus() == MemberStatus.DEACTIVATED) {
            return changeStatusMembership(membership, MemberStatus.ACTIVATED, StatusAction.ACTIVATION);
        }
        return membership.getStatus().toString();
    }

    @Override
    public String expireMembership(UpdateMembership updateMembership) throws DataAccessException, MissingElementException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        return changeStatusMembership(membership, MemberStatus.EXPIRED, StatusAction.EXPIRATION);
    }

    @Override
    public String discardMembership(UpdateMembership updateMembership) throws MissingElementException, DataAccessException {
        final Membership membership = memberService.getMembershipByIdWithMemberAccount(Long.parseLong(updateMembership.getMembershipId()));
        return changeStatusMembership(membership, MemberStatus.DISCARDED, StatusAction.DISCARD);
    }

    @Override
    public boolean consumeMembershipRemnantBalance(UpdateMembership updateMembership) throws DataAccessException {
        try {
            return getMembershipStore().updateMembershipBalance(dataSource, Long.parseLong(updateMembership.getMembershipId()), BigDecimal.ZERO);
        } catch (SQLException e) {
            LOGGER.error("Error restoring membership balance to zero for membershipId {}", updateMembership.getMembershipId());
            throw new DataAccessRollbackException(
                    "Error trying to reset membership balance to zero for membershipId " + updateMembership.getMembershipId(), e);
        }
    }

    @Override
    public boolean activateRenewalPendingToCollect(Membership membership) throws DataAccessException {
        return getMembershipRenewalService().activatePendingToCollect(dataSource, membership);
    }

    private String changeAutoRenewal(Long memberId, MembershipRenewal desiredValue, MembershipRenewal oldValue, FunctionWithException<Long, Boolean, DataAccessException> function) throws MissingElementException, DataAccessException {
        MembershipRenewal membershipRenewal = getMemberManager().getMembershipById(dataSource, memberId).getAutoRenewal();
        return changeAutoRenewal(membershipRenewal, memberId, desiredValue, oldValue, function);
    }

    private String changeAutoRenewal(MembershipRenewal membershipRenewal, Long memberId, MembershipRenewal desiredValue, MembershipRenewal oldValue, FunctionWithException<Long, Boolean, DataAccessException> function) throws DataAccessException {
        if (membershipRenewal.equals(oldValue) && function.apply(memberId)) {
            getMembershipMessageSendingManager().sendMembershipIdToMembershipReporter(memberId);
            return desiredValue.toString();
        }
        return membershipRenewal.toString();
    }

    private String changeStatusMembership(Membership membership, MemberStatus desiredNewStatus, StatusAction statusAction) throws DataAccessException {
        MemberStatus previousStatus = membership.getStatus();
        MemberStatus newStatus = performStatusChange(membership, desiredNewStatus);
        if (newStatus.equals(desiredNewStatus)) {
            storeStatusAction(membership.getId(), statusAction);
            getMembershipMessageSendingManager().sendSubscriptionMessageToCRMTopicByRule(previousStatus, newStatus, membership);
            getMembershipMessageSendingManager().sendMembershipIdToMembershipReporter(membership.getId());
        }
        return newStatus.toString();
    }

    private void storeStatusAction(Long membershipId, StatusAction statusAction) throws DataAccessRollbackException {
        try {
            getMemberStatusActionStore().createMemberStatusAction(dataSource, membershipId, statusAction);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + statusAction.toString() + " for membershipId " + membershipId, e);
        }
    }


    private boolean addRecurringId(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, ExistingRecurringException {
        try {
            final long membershipId = Long.parseLong(updateMembership.getMembershipId());
            validateMembershipExistsAndDoesNotHaveRecurring(membershipId);
            String recurringId = Optional.ofNullable(updateMembership.getRecurringId())
                    .filter(StringUtils::isNotEmpty)
                    .orElseThrow(() -> new InvalidParameterException("Recurring id can't be null or empty string"));
            getMembershipRecurringStore().insertMembershipRecurring(dataSource, membershipId, recurringId);
        } catch (SQLException e) {
            throw new DataAccessException("Error inserting the recurring ID for membership ID : " + updateMembership.getMembershipId(), e);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Membership ID : " + updateMembership.getMembershipId() + " not available.", e);
        }
        return Boolean.TRUE;
    }

    private void validateMembershipExistsAndDoesNotHaveRecurring(long membershipId) throws SQLException, DataNotFoundException, ExistingRecurringException {
        final Membership membership = Optional.ofNullable(getMembershipStore().fetchMembershipById(dataSource, membershipId))
                .orElseThrow(() -> new DataNotFoundException("Membership not found"));
        if (StringUtils.isNotEmpty(membership.getRecurringId())) {
            throw new ExistingRecurringException("Recurring id already exists for membership " + membership.getId());
        }
    }

    private MemberStatus performStatusChange(Membership membership, MemberStatus desiredStatus) throws DataAccessException {
        MemberStatus status = membership.getStatus();
        boolean isTransitionAllowed = status.isStatusTransitionAllowed(desiredStatus);
        if (isTransitionAllowed && getMembershipStore().updateStatusAndAutorenewal(dataSource, membership.getId(), desiredStatus, getMembershipRenewalByStatusChange(membership, desiredStatus))) {
            return desiredStatus;
        }
        return status;
    }

    private MembershipRenewal getMembershipRenewalByStatusChange(Membership membership, MemberStatus status) {
        return Optional.ofNullable(transitionChangeStatusToAutoRenewal.get(status))
                .orElse(membership.getAutoRenewal());
    }

    private MembershipRenewalService getMembershipRenewalService() {
        return Optional.ofNullable(membershipRenewalService).orElseGet(() -> {
            membershipRenewalService = ConfigurationEngine.getInstance(MembershipRenewalService.class);
            return membershipRenewalService;
        });
    }
}

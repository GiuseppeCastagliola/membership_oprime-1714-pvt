package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.parameters.MembershipCreation;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface MembershipPostBookingService {
    List<String> parseInfoAndCreateAccounts(String csv);

    Long createPostBookingMembership(MembershipCreation membershipCreation, String email)
            throws DataAccessException, ActivatedMembershipException;

    Long createPostBookingMembershipPending(MembershipCreation membershipCreation)
            throws DataAccessException, ActivatedMembershipException;

    PostBookingPageInfo getPostBookingPageInfo(String token) throws UnsupportedEncodingException;
}

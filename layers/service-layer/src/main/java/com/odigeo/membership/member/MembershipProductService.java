package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MembershipFee;

import java.util.List;

public interface MembershipProductService {
    List<MembershipFee> getMembershipFees(Long membershipId) throws DataAccessException;
}

package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;

public interface MembershipValidationService {
    Boolean applyMembership(MemberOnPassengerListParameter memberOnPassengerListParameter) throws
            MissingElementException, DataAccessException;

    Boolean isMembershipActiveOnWebsite(String siteId);

    Boolean isMembershipToBeRenewed(Long membershipId) throws MissingElementException, DataAccessException;

    boolean isEligibleForFreeTrial(FreeTrialCandidateRequest freeTrialCandidateRequest) throws DataAccessException;
}

package com.odigeo.membership.fees;

import com.odigeo.product.v2.model.Fee;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface MembershipFeesService {

    List<Fee> retrieveFees(Long feeContainerId);

    Fee fillProductFee(BigDecimal amount, String currency);

    Optional<Long> requestFeeContainerCreation(BigDecimal subscriptionPrice, String currencyCode);

}

package com.odigeo.membership.product;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

import java.util.Arrays;
import java.util.List;

@Singleton
@ConfiguredInPropertiesFile
public class MembershipSubscriptionConfiguration {

    private String[] activeSites;
    private String[] monthlySubscriptionMarkets;

    public List<String> getActiveSitesList() {
        return Arrays.asList(activeSites);
    }

    public void setActiveSites(String... activeSites) {
        this.activeSites = activeSites.clone();
    }

    public List<String> getMonthlySubscriptionMarketsList() {
        return Arrays.asList(monthlySubscriptionMarkets);
    }

    public void setMonthlySubscriptionMarkets(String... monthlySubscriptionMarkets) {
        this.monthlySubscriptionMarkets = monthlySubscriptionMarkets.clone();
    }
}

package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.commons.messaging.PublishMessageException;

public interface BackOfficeService {

    void updateMembershipMarketingInfo(Long membershipId) throws MissingElementException, DataAccessException, PublishMessageException;

    boolean sendWelcomeEmail(Long membershipId, Long bookingId) throws MissingElementException, DataAccessException;
}

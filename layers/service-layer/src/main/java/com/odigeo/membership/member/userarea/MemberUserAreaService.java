package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.bookingapi.v14.responses.BookingDetail;
import com.odigeo.membership.MemberAccountBooking;
import com.odigeo.membership.MemberSubscriptionDetails;
import com.odigeo.membership.exception.bookingapi.BookingApiException;

import java.util.List;

public interface MemberUserAreaService {

    List<MemberSubscriptionDetails> getAllMemberSubscriptionDetails(long memberAccountId) throws MissingElementException, DataAccessException;

    MemberSubscriptionDetails getMemberSubscriptionDetails(long membershipId);

    BookingDetail getBookingDetailSubscription(long membershipId) throws BookingApiException;

    boolean userHasAnyMembershipForBrand(long userId, String brandCode) throws DataAccessException;

    MemberAccountBooking getMembershipInfo(long membershipId, Boolean skipBooking) throws MissingElementException, DataAccessException, BookingApiException;
}

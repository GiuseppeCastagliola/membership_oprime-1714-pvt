package com.odigeo.membership.propertiesconfig;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.Serializable;

import static java.util.Objects.nonNull;

@Singleton
public class PropertiesCacheService {

    private static final Serializable SEND_IDS_TO_KAFKA_CACHE_KEY = "sendIdsKafkaActive";
    private static final Serializable SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY = "sendTransactionalWelcomeEmailActive";
    private final PropertiesCacheConfiguration propertiesCacheConfiguration;
    private final PropertiesConfigurationService propertiesConfigurationService;

    @Inject
    public PropertiesCacheService(final PropertiesCacheConfiguration propertiesCacheConfiguration, final PropertiesConfigurationService propertiesConfigurationService) {
        this.propertiesCacheConfiguration = propertiesCacheConfiguration;
        this.propertiesConfigurationService = propertiesConfigurationService;
    }

    public boolean isSendingIdsToKafkaActive() {
        Boolean sendIdsToKafkaProperty = (Boolean) propertiesCacheConfiguration.getIsSendingIdsToKafkaActiveCache().fetchValue(SEND_IDS_TO_KAFKA_CACHE_KEY);
        return nonNull(sendIdsToKafkaProperty) ? sendIdsToKafkaProperty : refreshIsSendingIdsToKafkaActive();
    }

    boolean refreshIsSendingIdsToKafkaActive() {
        boolean sendIdsToKafka = propertiesConfigurationService.isSendingIdsToKafkaActive();
        propertiesCacheConfiguration.getIsSendingIdsToKafkaActiveCache().addEntry(SEND_IDS_TO_KAFKA_CACHE_KEY, sendIdsToKafka);
        return sendIdsToKafka;
    }

    public boolean isTransactionalWelcomeEmailActive() {
        Boolean transactionalWelcomeEmailActive = (Boolean) propertiesCacheConfiguration.getIsTransactionalWelcomeEmailActiveCache().fetchValue(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY);
        return nonNull(transactionalWelcomeEmailActive) ? transactionalWelcomeEmailActive : refreshIsTransactionalWelcomeEmailActive();
    }

    boolean refreshIsTransactionalWelcomeEmailActive() {
        boolean sendTransactionalWelcomeEmail = propertiesConfigurationService.isTransactionalWelcomeEmailActive();
        propertiesCacheConfiguration.getIsTransactionalWelcomeEmailActiveCache().addEntry(SEND_TRANSACTIONAL_WELCOME_EMAIL_CACHE_KEY, sendTransactionalWelcomeEmail);
        return sendTransactionalWelcomeEmail;
    }

    void refreshAllProperties() {
        refreshIsSendingIdsToKafkaActive();
        refreshIsTransactionalWelcomeEmailActive();
    }
}

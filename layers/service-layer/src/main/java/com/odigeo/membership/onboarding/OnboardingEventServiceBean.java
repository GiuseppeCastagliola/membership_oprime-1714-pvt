package com.odigeo.membership.onboarding;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.AbstractServiceBean;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.sql.SQLException;
import java.util.Optional;

@Stateless
@Local(OnboardingEventService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OnboardingEventServiceBean extends AbstractServiceBean implements OnboardingEventService {

    private OnboardingStore onboardingStore;

    @Override
    public long createOnboarding(Onboarding onboarding) throws DataAccessException {
        try {
            return getOnboardingStore().createOnboarding(dataSource, onboarding.getMemberAccountId(), onboarding.getEvent(), onboarding.getDevice());
        } catch (SQLException e) {
            throw new DataAccessRollbackException("There was an error trying to create a new onboarding: " + onboarding, e);
        }
    }

    private OnboardingStore getOnboardingStore() {
        return Optional.ofNullable(onboardingStore).orElseGet(() -> {
            onboardingStore = ConfigurationEngine.getInstance(OnboardingStore.class);
            return onboardingStore;
        });
    }
}

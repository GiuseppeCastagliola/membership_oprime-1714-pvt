package com.odigeo.membership.auth.usecases;

import com.google.inject.Inject;
import com.odigeo.authapi.AuthContract;
import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public class AuthenticationLoginUseCaseImpl implements AuthenticationLoginUseCase {

    @Inject
    private AuthContract authContract;

    @Override
    public AuthenticationResponse login(final String userName, final String password) throws MembershipAuthServiceException {
        return authContract.login(userName, password);
    }
}

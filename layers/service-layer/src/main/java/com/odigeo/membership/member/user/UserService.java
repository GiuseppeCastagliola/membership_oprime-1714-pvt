package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.parameters.UserCreation;

public interface UserService {

    Long saveUser(UserCreation userCreation) throws DataAccessException;
}

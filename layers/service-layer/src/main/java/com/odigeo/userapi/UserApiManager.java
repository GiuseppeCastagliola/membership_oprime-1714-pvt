package com.odigeo.userapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.userprofiles.api.v1.UserServiceInternalManager;
import com.odigeo.userprofiles.api.v1.model.Brand;
import com.odigeo.userprofiles.api.v1.model.HashCode;
import com.odigeo.userprofiles.api.v1.model.HashType;
import com.odigeo.userprofiles.api.v1.model.InternalServerException;
import com.odigeo.userprofiles.api.v1.model.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v1.model.InvalidFindException;
import com.odigeo.userprofiles.api.v1.model.InvalidValidationException;
import com.odigeo.userprofiles.api.v1.model.RemoteException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

@Singleton
public class UserApiManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiManager.class);
    private final UserServiceInternalManager userServiceInternalManager;

    @Inject
    public UserApiManager(UserServiceInternalManager userServiceInternalManager) {
        this.userServiceInternalManager = userServiceInternalManager;
    }

    public Optional<String> getForgetPasswordToken(long userId) {
        try {
            return userServiceInternalManager.getHashCodes(userId, HashType.REQUEST_PASSWORD)
                    .stream()
                    .findFirst()
                    .map(HashCode::getCode);
        } catch (InvalidFindException | RemoteException | InvalidCredentialsException | InternalServerException e) {
            LOGGER.error("Error while retrieving REQUEST_PASSWORD HashCodes for userId: " + userId, e);
        }
        return Optional.empty();
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public UserInfo getUserInfo(long userId) {
        try {
            return Optional.ofNullable(userServiceInternalManager.getUserBasicInfo(userId))
                    .map(UserInfo::fromUserBasicInfo)
                    .orElse(UserInfo.defaultUserInfo());
        } catch (RemoteException | InternalServerException | InvalidCredentialsException | InvalidFindException e) {
            LOGGER.error("Error while retrieving UserBasicInfo for userId: " + userId, e);
        } catch (Exception e) {
            LOGGER.error("Generic error while retrieving UserBasicInfo for userId: " + userId, e);
        }
        return UserInfo.defaultUserInfo();
    }

    public UserInfo getUserInfo(String email, String website) {
        try {
            Optional<Brand> brand = BrandMapper.map(website);
            if (brand.isPresent()) {
                return Optional.ofNullable(userServiceInternalManager.getUserBasicInfo(email, brand.get()))
                        .map(UserInfo::fromUserBasicInfo)
                        .orElse(UserInfo.defaultUserInfo());
            }
            LOGGER.warn("Error while retrieving brand from email: {}, website: {}", email, website);
        } catch (InvalidFindException | RemoteException | InvalidCredentialsException | InternalServerException | InvalidValidationException e) {
            LOGGER.error("Error while retrieving UserBasicInfo for email: " + email + "and website: " + website, e);
        }
        return UserInfo.defaultUserInfo();
    }

    public String getEmail(long userId) throws DataNotFoundException {
        return Optional.ofNullable(getUserInfo(userId).getEmail())
                .orElseThrow(() -> new DataNotFoundException("User api returned email null for userId " + userId));
    }
}

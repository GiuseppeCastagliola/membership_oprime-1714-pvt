package com.odigeo.commons.monitoring.metrics;

import com.odigeo.membership.AutorenewalTracking;

public final class MetricsBuilder {

    private MetricsBuilder() {
    }

    public static Metric buildMetric(String constant) {
        return new Metric.Builder(constant)
                .build();
    }

    public static Metric buildActivationRetryAttemptNumberMetric(String metricName, Integer attemptNumber) {
        return new Metric.Builder(metricName)
                .tag(MetricsNames.ATTEMPT_NUMBER, attemptNumber.toString())
                .build();
    }

    public static Metric buildTrackingRetryAttemptNumberMetric(String metricName, Integer attemptNumber) {
        return new Metric.Builder(metricName)
                .tag(MetricsNames.TRACKING_RETRY_NUMBER, attemptNumber.toString())
                .build();
    }

    public static Metric composeResponseTimeMetric(String operation) {
        return new Metric.Builder(MetricsNames.METRICS_TIME_TABLE_NAME)
                .tag(MetricsNames.OPERATION, operation)
                .build();
    }

    public static Metric buildRecurringByWebsite(String website) {
        return new Metric.Builder(MetricsNames.BOOKING_MEMBERSHIP_RECURRING_BY_WEBSITE)
                .tag(MetricsNames.WEBSITE, website)
                .build();
    }

    public static Metric buildFreeTrialEligibleByWebsite(boolean isEligible, String website) {
        return new Metric.Builder(isEligible ? MetricsNames.USER_ELIGIBLE_SUCCESS : MetricsNames.USER_ELIGIBLE_BLOCKED)
                .tag(MetricsNames.WEBSITE, website)
                .build();
    }

    public static Metric buildFailProcessRecurring(String errorMessage) {
        return new Metric.Builder(MetricsNames.FAIL_RECURRING)
                .tag(MetricsNames.ERROR_RECURRING, errorMessage)
                .build();
    }

    public static Metric buildAutorenewalBySource(AutorenewalTracking autorenewalTracking) {
        return new Metric.Builder(MetricsNames.MEMBERSHIP_AUTORENEWAL)
                .tag(MetricsNames.AUTORENEWAL_REQUESTER, autorenewalTracking.getRequester())
                .tag(MetricsNames.AUTORENEWAL_OPERATION, autorenewalTracking.getAutoRenewalOperation().toString())
                .tag(MetricsNames.AUTORENEWAL_METHOD, autorenewalTracking.getRequestedMethod())
                .tag(MetricsNames.AUTORENEWAL_MEMBERSHIP_ID, String.valueOf(autorenewalTracking.getMembershipId()))
                .build();
    }

    public static Metric buildFailedSendMessageToCRM() {
        return new Metric.Builder(MetricsNames.FAILED_CRM_MSG)
                .build();
    }
}

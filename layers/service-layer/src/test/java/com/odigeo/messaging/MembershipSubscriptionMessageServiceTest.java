package com.odigeo.messaging;

import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.kafka.MembershipKafkaSender;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.Status;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.odigeo.membership.MemberStatus.PENDING_TO_ACTIVATE;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipSubscriptionMessageServiceTest {
    private static final String EMAIL = "user@odigeo.com";
    private static final String WEBSITE = "website";
    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long USER_ID = 343L;
    private static final Long BOOKING_ID = 200L;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = ACTIVATION_DATE.plusMonths(6);
    private static final int MONTHS_DURATION = 6;
    private static final Membership NULL_MEMBER_ACCOUNT = new MembershipBuilder().build();
    private static final Membership PENDING_TO_ACTIVATE_MEMBERSHIP = new MembershipBuilder()
            .setStatus(MemberStatus.PENDING_TO_ACTIVATE)
            .build();

    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
    @Captor
    private ArgumentCaptor<MembershipSubscriptionMessage> membershipSubscriptionMessageCaptor;
    @Mock
    private UserApiManager userApiManagerMock;
    @Mock
    private MembershipKafkaSender membershipKafkaSenderMock;
    @Mock
    private MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapperMock;

    private MembershipSubscriptionMessageService membershipSubscriptionMessageService;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        membershipSubscriptionMessageService = new MembershipSubscriptionMessageService(userApiManagerMock, membershipKafkaSenderMock, memberStatusToCrmStatusMapperMock);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicByRule() throws PublishMessageException {
        final MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        final Membership expiredMembership = new MembershipBuilder().setWebsite(WEBSITE)
                .setExpirationDate(LocalDateTime.now())
                .setMonthsDuration(MONTHS_DURATION)
                .setStatus(MemberStatus.EXPIRED)
                .setMemberAccount(memberAccount)
                .build();
        when(memberStatusToCrmStatusMapperMock.mapForUpdatedMembership(eq(expiredMembership), eq(MemberStatus.EXPIRED))).thenReturn(SubscriptionStatus.UNSUBSCRIBED);
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopicByRule(MemberStatus.ACTIVATED, MemberStatus.EXPIRED, expiredMembership);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessageForExpired(), membershipSubscriptionMessageCaptor.getValue());
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicByRule_NullMemberAccount() throws PublishMessageException {
        final Membership expiredMembership = new MembershipBuilder().setWebsite(WEBSITE)
                .setExpirationDate(LocalDateTime.now())
                .setStatus(MemberStatus.EXPIRED)
                .build();
        when(memberStatusToCrmStatusMapperMock.mapForUpdatedMembership(eq(expiredMembership), eq(MemberStatus.EXPIRED))).thenReturn(SubscriptionStatus.UNSUBSCRIBED);
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopicByRule(MemberStatus.ACTIVATED, MemberStatus.EXPIRED, expiredMembership);
        verify(membershipKafkaSenderMock, never()).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
        verify(userApiManagerMock, never()).getUserInfo(anyLong());
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicByRule_KafkaSenderThrowsPublishException() throws PublishMessageException {
        final MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        final Membership expiredMembership = new MembershipBuilder().setWebsite(WEBSITE)
                .setExpirationDate(LocalDateTime.now())
                .setStatus(MemberStatus.EXPIRED)
                .setMemberAccount(memberAccount)
                .build();
        when(memberStatusToCrmStatusMapperMock.mapForUpdatedMembership(eq(expiredMembership), eq(MemberStatus.EXPIRED))).thenReturn(SubscriptionStatus.UNSUBSCRIBED);
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException()))
                .when(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopicByRule(MemberStatus.ACTIVATED, MemberStatus.EXPIRED, expiredMembership);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicByRuleNotCompliant() throws PublishMessageException {
        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopicByRule(MemberStatus.ACTIVATED, PENDING_TO_ACTIVATE, PENDING_TO_ACTIVATE_MEMBERSHIP);
        verify(membershipKafkaSenderMock, never()).sendMembershipSubscriptionMessageToKafka(any());
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicHappyPathWithSuccessfulCRMMsg() throws PublishMessageException {
        final Membership membership = getMembership();
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        assertTrue(membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(membership, SubscriptionStatus.SUBSCRIBED));
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessage(), membershipSubscriptionMessageCaptor.getValue());
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicHappyPathWithNoActivationDate() throws PublishMessageException {
        final Membership membership = getPendingToCollectMembership();
        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));

        assertTrue(membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(membership, SubscriptionStatus.SUBSCRIBED));
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessageForPTC(), membershipSubscriptionMessageCaptor.getValue());
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicHappyPathWithFailedCRMMsg() throws PublishMessageException {
        final Membership membership = getMembership();

        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false)));
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException()))
                .when(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));

        assertFalse(membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(membership, SubscriptionStatus.SUBSCRIBED));
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        assertEqualsMembershipSubscriptionFields(sampleMembershipSubscriptionMessage(), membershipSubscriptionMessageCaptor.getValue());
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopic_NullMemberAccount() throws PublishMessageException {
        assertFalse(membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(NULL_MEMBER_ACCOUNT, SubscriptionStatus.SUBSCRIBED));
        verify(membershipKafkaSenderMock, never()).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
        verify(userApiManagerMock, never()).getUserInfo(anyLong());
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopic_NullEmail() throws PublishMessageException {
        final Membership membership = getMembership();

        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(UserInfo.defaultUserInfo());

        assertFalse(membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(membership, SubscriptionStatus.SUBSCRIBED));
        verify(membershipKafkaSenderMock, never()).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_HappyPath() throws PublishMessageException {
        final Membership membership = getMembership();
        final UserInfo userInfo = getUserInfo();

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(userInfo);

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(userInfo, membership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_ExceptionRetrievingUserInfo() throws PublishMessageException {
        final Membership membership = getMembership();
        final UserInfo userInfoNullEmail = UserInfo.defaultUserInfo();

        when(userApiManagerMock.getUserInfo(USER_ID)).thenReturn(userInfoNullEmail);

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(userInfoNullEmail, membership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock, never()).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
        verifyLogEventsWhenExceptionOccurred();
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_KafkaSenderThrowsException() throws PublishMessageException {
        final Membership membership = getMembership();
        final UserInfo userInfo = getUserInfo();

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(userInfo);
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException()))
                .when(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(userInfo, membership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(any(MembershipSubscriptionMessage.class));
        verifyLogEventsWhenExceptionOccurred();
    }

    @DataProvider(name = "userInfosNoForgotPwdTokenInSubscriptionMessage")
    public Object[][] userInfosNoForgotPwdTokenInSubscriptionMessage() {
        return new Object[][]{
                {UserInfo.defaultUserInfo()},
                {UserInfo.fromUserBasicInfo(sampleUserBasicInfo(false))},
        };
    }

    @Test(dataProvider = "userInfosNoForgotPwdTokenInSubscriptionMessage")
    public void testSendMembershipSubscriptionMessage_ForgetPasswordTokenNotAdded(UserInfo userInfo) throws PublishMessageException {
        final MembershipCreation membershipCreation = getMembershipCreation();
        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(userInfo);

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(EMAIL, membershipCreation, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        MembershipSubscriptionMessage sentMembershipSubscriptionMessage = membershipSubscriptionMessageCaptor.getValue();

        assertEqualsMembershipSubscriptionFields(sentMembershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        verify(userApiManagerMock, never()).getForgetPasswordToken(anyLong());
        assertNull(sentMembershipSubscriptionMessage.getForgetPasswordToken());
        assertFalse(sentMembershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test
    public void testSendMembershipSubscriptionMessage_WithPendingLoginUserAndNoToken() throws PublishMessageException {
        final MembershipCreation membershipCreation = getMembershipCreation();
        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(true)));
        when(userApiManagerMock.getForgetPasswordToken(USER_ID)).thenReturn(Optional.empty());

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(EMAIL, membershipCreation, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        MembershipSubscriptionMessage sentMembershipSubscriptionMessage = membershipSubscriptionMessageCaptor.getValue();

        assertEqualsMembershipSubscriptionFields(sentMembershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        verify(userApiManagerMock).getForgetPasswordToken(anyLong());
        assertNull(sentMembershipSubscriptionMessage.getForgetPasswordToken());
        assertFalse(sentMembershipSubscriptionMessage.getShouldSetPassword());
    }

    @Test
    public void testSendMembershipSubscriptionMessage_WithPendingLoginUserAndToken() throws PublishMessageException {
        final String token = "token";
        final MembershipCreation membershipCreation = getMembershipCreation();
        final MembershipSubscriptionMessage defaultMembershipSubscriptionMessage = sampleMembershipSubscriptionMessage();

        when(userApiManagerMock.getUserInfo(EMAIL, WEBSITE)).thenReturn(UserInfo.fromUserBasicInfo(sampleUserBasicInfo(true)));
        when(userApiManagerMock.getForgetPasswordToken(USER_ID)).thenReturn(Optional.of(token));

        membershipSubscriptionMessageService.sendSubscriptionMessageToCRMTopic(EMAIL, membershipCreation, SubscriptionStatus.SUBSCRIBED);
        verify(membershipKafkaSenderMock).sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessageCaptor.capture());
        MembershipSubscriptionMessage sentMembershipSubscriptionMessage = membershipSubscriptionMessageCaptor.getValue();

        assertEqualsMembershipSubscriptionFields(sentMembershipSubscriptionMessage, defaultMembershipSubscriptionMessage);
        verify(userApiManagerMock).getForgetPasswordToken(anyLong());
        assertEquals(sentMembershipSubscriptionMessage.getForgetPasswordToken(), token);
        assertTrue(sentMembershipSubscriptionMessage.getShouldSetPassword());
    }

    private Membership getMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        return new MembershipBuilder().setWebsite(WEBSITE)
                .setActivationDate(ACTIVATION_DATE).setExpirationDate(EXPIRATION_DATE)
                .setMemberAccount(memberAccount).build();
    }

    private Membership getPendingToCollectMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, USER_ID, "test", "test");
        return new MembershipBuilder().setWebsite(WEBSITE)
                .setStatus(MemberStatus.PENDING_TO_COLLECT)
                .setMonthsDuration(MONTHS_DURATION)
                .setExpirationDate(EXPIRATION_DATE)
                .setMemberAccount(memberAccount).build();
    }

    private MembershipCreation getMembershipCreation() {
        return new MembershipCreationBuilder()
                .withWebsite(WEBSITE)
                .withActivationDate(ACTIVATION_DATE)
                .withExpirationDate(EXPIRATION_DATE)
                .build();
    }

    private void assertEqualsMembershipSubscriptionFields(MembershipSubscriptionMessage expected, MembershipSubscriptionMessage actual) {
        assertEquals(actual.getEmail(), expected.getEmail());
        assertEquals(actual.getSubscriptionStatus(), expected.getSubscriptionStatus());
        assertEquals(actual.getActivationDate(), expected.getActivationDate());
        assertEquals(actual.getExpirationDate(), expected.getExpirationDate());
        assertEquals(actual.getWebsite(), expected.getWebsite());
        assertEquals(actual.getKey(), expected.getKey());
        assertEquals(actual.getTopicName(), expected.getTopicName());
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessage() {
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED)
                .withDates(ACTIVATION_DATE.toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .build();
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessageForPTC() {
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED)
                .withDates(EXPIRATION_DATE.minusMonths(MONTHS_DURATION).toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .build();
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessageForExpired() {
        LocalDate expirationToday = LocalDate.now();
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.UNSUBSCRIBED)
                .withDates(expirationToday.minusMonths(MONTHS_DURATION), expirationToday)
                .build();
    }

    private UserBasicInfo sampleUserBasicInfo(boolean isPendingLogin) {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setStatus(isPendingLogin ? Status.PENDING_LOGIN : Status.ACTIVE);
        return userBasicInfo;
    }

    private UserInfo getUserInfo() {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setId(USER_ID);
        return UserInfo.fromUserBasicInfo(userBasicInfo);
    }

    private void verifyLogEventsWhenExceptionOccurred() {
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        assertEquals(loggingEvents.size(), 1);
        assertEquals(Level.WARN, loggingEvents.get(0).getLevel());
    }
}

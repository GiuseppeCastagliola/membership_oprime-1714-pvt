package com.odigeo.util;

import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.MembershipFee;
import com.odigeo.product.v2.model.Fee;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class FeeMapperTest {

    private static final String PRODUCT_TYPE = "PRODUCT_TYPE";
    private static final String CURRENCY_CODE = "USD";
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private FeeMapper feeMapper;

    @BeforeMethod
    public void setUp() {
        feeMapper = new FeeMapper();
    }

    @Test(dataProvider = "provideAbstractFees")
    public void testAbstractFeeToProductFee(AbstractFee abstractFee) {
        Fee fee = feeMapper.abstractFeeToProductFee(abstractFee);
        assertEquals(fee.getPrice().getAmount(), SUBSCRIPTION_PRICE);
        assertEquals(fee.getPrice().getCurrency(), Currency.getInstance(CURRENCY_CODE));
        assertEquals(fee.getLabel(), FeeLabel.MARKUP_TAX.toString());
    }

    @Test(dataProvider = "provideAbstractFees")
    public void testAbstractFeeToMembershipFee(AbstractFee abstractFee) {
        MembershipFee membershipFee = feeMapper.abstractFeeToMembershipFee(abstractFee);
        assertEquals(membershipFee.getAmount(), SUBSCRIPTION_PRICE);
        assertEquals(membershipFee.getCurrency(), CURRENCY_CODE);
        assertEquals(membershipFee.getSubCode(), FeeMapper.MEMBERSHIP_RENEWAL_FEE_SUBCODE);
    }

    @Test
    public void testMembershipFeeToFeeLabelMap() {
        Map<FeeLabel, List<AbstractFee>> abstractFeesByLabel = feeMapper.membershipFeeToFeeLabelMap(SUBSCRIPTION_PRICE, CURRENCY_CODE);
        assertEquals(abstractFeesByLabel.get(FeeLabel.MARKUP_TAX).size(), 1);
    }

    @DataProvider
    private static Object[][] provideAbstractFees() {
        FixFee fixFee = new FixFee();
        fixFee.setCurrency(Currency.getInstance(CURRENCY_CODE));
        fixFee.setAmount(SUBSCRIPTION_PRICE);
        fixFee.setFeeLabel(FeeLabel.MARKUP_TAX);
        fixFee.setSubCode(FeeMapper.MEMBERSHIP_RENEWAL_FEE_SUBCODE);
        return new AbstractFee[][]{
                new AbstractFee[]{fixFee}
        };
    }

}

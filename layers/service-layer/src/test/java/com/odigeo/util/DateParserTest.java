package com.odigeo.util;

import org.testng.annotations.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Date;
import java.util.Optional;

import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;

public class DateParserTest {

    @Test
    public void testEmptyDateTimeWhenLocalDateTimeNull() {
        //When
        String isoDate = DateParser.toIsoDateTime(null);
        //Then
        assertEquals(isoDate, EMPTY);
    }

    @Test
    public void testStringIsoDateTimeReturnedWhenValidLocalDateTime() {
        //Given
        LocalDateTime localDateTime = LocalDateTime.of(2019, Month.DECEMBER, 21, 8, 9, 18);
        //When
        String isoDate = DateParser.toIsoDateTime(localDateTime);
        //Then
        assertEquals(isoDate, "2019-12-21T08:09:18");
    }

    @Test
    public void testEmptyDateWhenLocalDateTimeNull() {
        //When
        String isoDate = DateParser.toIsoDate((LocalDateTime) null);
        //Then
        assertEquals(isoDate, EMPTY);
    }

    @Test
    public void testStringIsoDateReturnedWhenValidLocalDateTime() {
        //Given
        LocalDateTime localDateTime = LocalDateTime.of(2018, Month.FEBRUARY, 16, 8, 9);
        //When
        String isoDate = DateParser.toIsoDate(localDateTime);
        //Then
        assertEquals(isoDate, "2018-02-16");
    }

    @Test
    public void testEmptyDateWhenDateNull() {
        //When
        Optional<String> isoDate = DateParser.toIsoDate((Date) null);
        //Then
        assertFalse(isoDate.isPresent());
    }

    @Test
    public void testStringIsoDateReturnedWhenValidDate() {
        //Given
        Date date = Date.from(Instant.parse("2007-12-03T10:15:30.00Z"));
        //When
        Optional<String> isoDate = DateParser.toIsoDate(date);
        //Then
        if (isoDate.isPresent()){
            assertEquals(isoDate.get(), "2007-12-03");
        } else {
            fail("Expected date not found.");
        }
    }
}
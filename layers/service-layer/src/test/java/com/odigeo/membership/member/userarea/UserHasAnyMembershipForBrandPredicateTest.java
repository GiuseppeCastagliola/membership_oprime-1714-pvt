package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.google.common.collect.Sets;
import com.odigeo.membership.member.MemberAccountService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class UserHasAnyMembershipForBrandPredicateTest {

    private final MemberAccountService memberAccountServiceMock = mock(MemberAccountService.class);
    private UserHasAnyMembershipForBrandPredicate predicate;

    @BeforeMethod
    public void setUp() {
        predicate = new UserHasAnyMembershipForBrandPredicate(memberAccountServiceMock);
    }

    @Test
    public void testFalseWhenUserHasNoActivatedMembership() throws DataAccessException {
        //Given
        final long userId = 102L;
        givenUserSites(userId);
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "OP");
        //Then
        assertFalse(hasAnyMembership);
    }

    @Test
    public void testTrueWhenUserHasOneActivatedMembership() throws DataAccessException {
        //Given
        final long userId = 123L;
        givenUserSites(userId, "whichever site");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "ED");
        //Then
        assertFalse(hasAnyMembership);
    }

    @Test
    public void testFalseWhenUserHasSeveralActivatedMembershipDifferentBrandWithPrefix() throws DataAccessException {
        //Given
        final long userId = 408L;
        givenUserSites(userId, "OPES", " GOFR");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "ED");
        //Then
        assertFalse(hasAnyMembership);
    }

    @Test
    public void testFalseWhenUserHasSeveralActivatedMembershipDifferentBrandWithoutPrefix() throws DataAccessException {
        //Given
        final long userId = 638L;
        givenUserSites(userId, "ES", "IT", " GOFR");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "OP");
        //Then
        assertFalse(hasAnyMembership);
    }

    @Test
    public void testTrueWhenUserHasSeveralActivatedMembershipSameBrandWithoutPrefix() throws DataAccessException {
        //Given
        final long userId = 340L;
        givenUserSites(userId, "ES", "IT", "DE ");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "ED");
        //Then
        assertTrue(hasAnyMembership);
    }

    @Test
    public void testTrueWhenUserHasSeveralActivatedMembershipSameBrandWithPrefix() throws DataAccessException {
        //Given
        final long userId = 234L;
        givenUserSites(userId, "XXFR", "XXIT", " XXXX");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "XX");
        //Then
        assertTrue(hasAnyMembership);
    }

    @Test
    public void testTrueAndCaseInsensitiveWhenUserHasSeveralActivatedMembershipSameBrand() throws DataAccessException {
        //Given
        final long userId = 8334L;
        givenUserSites(userId, "opfR");
        //When
        boolean hasAnyMembership = predicate.testAnyMembershipForBrand(userId, "oP")
                && predicate.testAnyMembershipForBrand(userId, "Op")
                && predicate.testAnyMembershipForBrand(userId, "op")
                && predicate.testAnyMembershipForBrand(userId, "OP");
        //Then
        assertTrue(hasAnyMembership);
    }

    private void givenUserSites(final long userId, String... sites) throws DataAccessException {
        given(memberAccountServiceMock.getActiveMembershipSitesByUserId(userId)).willReturn(Sets.newHashSet(sites));
    }
}

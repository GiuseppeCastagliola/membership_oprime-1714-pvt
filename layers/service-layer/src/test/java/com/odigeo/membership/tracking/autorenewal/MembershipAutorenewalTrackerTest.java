package com.odigeo.membership.tracking.autorenewal;

import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.AutorenewalTracking;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.member.UpdateMembershipServiceBean;
import com.odigeo.membership.tracking.RequestContext;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertSame;
import static org.testng.Assert.assertTrue;

public class MembershipAutorenewalTrackerTest {

    private static final String MEMBERSHIP_ID = "123";
    private static final String VISIT_INFORMATION_HEADER_CONTENT = "12c";
    private static final String MODULE_INFO_HEADER_CONTENT = "m1:m1";
    private static final String EXPECTED_REQUESTER = "M1";
    private static final String VISIT_INFORMATION_HEADER = "visitInformation";
    private static final String MODULE_INFO_HEADER = "odigeo-module-info";
    private static final String EXPECTED_UNKNOWN_MODULE_NAME = "UNKNOWN MODULE";
    private static final String EXPECTED_METHOD = "UPDATE_MEMBERSHIP";
    private static final String ENABLE_AUTO_RENEWAL_EXPECTED_METHOD = "ENABLE_AUTO_RENEWAL";
    private static final String REACTIVATE_MEMBERSHIP_EXPECTED_METHOD = "REACTIVATE_MEMBERSHIP";
    private static final String DISABLE_AUTO_RENEWAL_EXPECTED_METHOD = "DISABLE_AUTO_RENEWAL";
    private static final String DEACTIVATE_MEMBERSHIP_EXPECTED_METHOD = "DEACTIVATE_MEMBERSHIP";

    MembershipAutorenewalTracker membershipAutorenewalTracker;
    @Mock
    private AutorenewalTrackingService autorenewalTrackingService;
    @Mock
    private InvocationContext context;
    @Mock
    private RequestContext requestContext;
    @Captor
    private ArgumentCaptor<InvocationContext> contextCaptor;
    @Captor
    private ArgumentCaptor<AutorenewalTracking> autorenewalTrackingArgumentCaptor;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipAutorenewalTracker = spy(new MembershipAutorenewalTracker());
        doReturn(autorenewalTrackingService).when(membershipAutorenewalTracker).getAutorenewalTrackingService();
        doReturn(requestContext).when(membershipAutorenewalTracker).getRequestContext();
    }

    @Test
    public void testAroundInvokeWithoutParameter() throws Exception {
        doReturn(new Object[]{}).when(context).getParameters();

        membershipAutorenewalTracker.aroundInvoke(context);

        verify(context).proceed();
        verify(membershipAutorenewalTracker).extractFields(contextCaptor.capture());
        verify(autorenewalTrackingService, never()).trackAutorenewalOperation(any());
        assertSame(contextCaptor.getValue(), context);
    }

    @Test
    public void testAroundInvoke() throws Exception {
        doReturn(new Object[]{createUpdateMembershipWithOperation(AutoRenewalOperation.DISABLE_AUTO_RENEW.toString())}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockEmptyHttpServletRequest();

        membershipAutorenewalTracker.aroundInvoke(context);

        verify(context).proceed();
        verify(membershipAutorenewalTracker).extractFields(contextCaptor.capture());
        verify(autorenewalTrackingService).trackAutorenewalOperation(any());
        assertSame(contextCaptor.getValue(), context);
    }

    @Test
    public void testExtractFieldsFullTracking() throws NoSuchMethodException {
        doReturn(new Object[]{createUpdateMembershipWithOperation(AutoRenewalOperation.DISABLE_AUTO_RENEW.toString())}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockHttpServletRequest();

        Optional<AutorenewalTracking> autorenewalTracking = membershipAutorenewalTracker.extractFields(context);

        assertTrue(autorenewalTracking.isPresent());
        assertEquals(autorenewalTracking.get().getAutoRenewalOperation(), AutoRenewalOperation.DISABLE_AUTO_RENEW);
        assertEquals(autorenewalTracking.get().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(autorenewalTracking.get().getRequestedMethod(), EXPECTED_METHOD);
        assertEquals(autorenewalTracking.get().getRequester(), EXPECTED_REQUESTER);
        assertEquals(autorenewalTracking.get().getVisitInformation(), VISIT_INFORMATION_HEADER_CONTENT);
        assertNull(autorenewalTracking.get().getInterfaceId());
    }

    @Test
    public void testExtractFieldsEmpty() throws NoSuchMethodException {
        UpdateMembership updateMembership = createUpdateMembershipWithOperation(null);
        doReturn(new Object[]{updateMembership}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockHttpServletRequest();

        Optional<AutorenewalTracking> autorenewalTracking = membershipAutorenewalTracker.extractFields(context);

        assertFalse(autorenewalTracking.isPresent());
    }

    @Test
    public void testAroundInvokeHeaders() throws Exception {
        doReturn(new Object[]{createUpdateMembershipWithOperation(AutoRenewalOperation.DISABLE_AUTO_RENEW.toString())}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockHttpServletRequest();

        membershipAutorenewalTracker.aroundInvoke(context);

        verify(context).proceed();
        verify(membershipAutorenewalTracker).extractFields(contextCaptor.capture());
        verify(autorenewalTrackingService).trackAutorenewalOperation(autorenewalTrackingArgumentCaptor.capture());
        assertSame(contextCaptor.getValue(), context);
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getVisitInformation(), VISIT_INFORMATION_HEADER_CONTENT);
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getRequester(), EXPECTED_REQUESTER);
    }

    @Test
    public void testAroundInvokeHeadersNull() throws Exception {
        doReturn(new Object[]{createUpdateMembershipWithOperation(AutoRenewalOperation.DISABLE_AUTO_RENEW.toString())}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockEmptyHttpServletRequest();

        membershipAutorenewalTracker.aroundInvoke(context);

        verify(context).proceed();
        verify(membershipAutorenewalTracker).extractFields(contextCaptor.capture());
        verify(autorenewalTrackingService).trackAutorenewalOperation(autorenewalTrackingArgumentCaptor.capture());
        assertSame(contextCaptor.getValue(), context);
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getVisitInformation(), "");
        assertEquals(autorenewalTrackingArgumentCaptor.getValue().getRequester(), EXPECTED_UNKNOWN_MODULE_NAME);
    }

    @Test
    public void testExtractFieldsWithoutOperation() throws Exception {
        UpdateMembership updateMembership = createUpdateMembershipWithOperation(StringUtils.EMPTY);
        doReturn(new Object[]{updateMembership}).when(context).getParameters();
        doReturn(createUpdateMembershipMethod()).when(context).getMethod();
        mockHttpServletRequest();

        Optional<AutorenewalTracking> autorenewalTracking = membershipAutorenewalTracker.extractFields(context);

        assertFalse(autorenewalTracking.isPresent());
    }

    @Test(dataProvider = "autoRenewalChangeMethods")
    public void testExtractFieldsWithOperation(Object method, UpdateMembership updateMembership, String methodExpected, AutoRenewalOperation autoRenewalOperationExpected) {
        doReturn(new Object[]{updateMembership}).when(context).getParameters();
        doReturn(method).when(context).getMethod();
        mockHttpServletRequest();

        Optional<AutorenewalTracking> autorenewalTracking = membershipAutorenewalTracker.extractFields(context);

        assertTrue(autorenewalTracking.isPresent());
        assertEquals(autorenewalTracking.get().getAutoRenewalOperation(), autoRenewalOperationExpected);
        assertEquals(autorenewalTracking.get().getMembershipId(), MEMBERSHIP_ID);
        assertEquals(autorenewalTracking.get().getRequestedMethod(), methodExpected);
        assertEquals(autorenewalTracking.get().getRequester(), EXPECTED_REQUESTER);
        assertEquals(autorenewalTracking.get().getVisitInformation(), VISIT_INFORMATION_HEADER_CONTENT);
        assertNull(autorenewalTracking.get().getInterfaceId());
    }

    @DataProvider(name = "autoRenewalChangeMethods")
    public Object[][] getEnabledAutoRenewalMethodsWithUpdateMembershipParameter() throws NoSuchMethodException {
        return new Object[][] {
                {createEnableAutoRenewalMethod(), createUpdateMembershipWithOperation(StringUtils.EMPTY), ENABLE_AUTO_RENEWAL_EXPECTED_METHOD, AutoRenewalOperation.ENABLE_AUTO_RENEW},
                {createReactivateStatusMethod(), createUpdateMembershipWithOperation(StringUtils.EMPTY), REACTIVATE_MEMBERSHIP_EXPECTED_METHOD, AutoRenewalOperation.ENABLE_AUTO_RENEW},
                {createEnableAutoRenewalMethod(), createUpdateMembershipWithOperation(null), ENABLE_AUTO_RENEWAL_EXPECTED_METHOD, AutoRenewalOperation.ENABLE_AUTO_RENEW},
                {createReactivateStatusMethod(), createUpdateMembershipWithOperation(null), REACTIVATE_MEMBERSHIP_EXPECTED_METHOD, AutoRenewalOperation.ENABLE_AUTO_RENEW},
                {createDisableAutoRenewalMethod(), createUpdateMembershipWithOperation(StringUtils.EMPTY), DISABLE_AUTO_RENEWAL_EXPECTED_METHOD, AutoRenewalOperation.DISABLE_AUTO_RENEW},
                {createDeactivateStatusMethod(), createUpdateMembershipWithOperation(StringUtils.EMPTY), DEACTIVATE_MEMBERSHIP_EXPECTED_METHOD, AutoRenewalOperation.DISABLE_AUTO_RENEW},
                {createDisableAutoRenewalMethod(), createUpdateMembershipWithOperation(null), DISABLE_AUTO_RENEWAL_EXPECTED_METHOD, AutoRenewalOperation.DISABLE_AUTO_RENEW},
                {createDeactivateStatusMethod(), createUpdateMembershipWithOperation(null), DEACTIVATE_MEMBERSHIP_EXPECTED_METHOD, AutoRenewalOperation.DISABLE_AUTO_RENEW}
        };
    }


    private UpdateMembership createUpdateMembershipWithOperation(String operation) {
        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setMembershipId(MEMBERSHIP_ID);
        updateMembership.setOperation(operation);
        return updateMembership;
    }

    private Method createUpdateMembershipMethod() throws NoSuchMethodException {
        Class<?> updateMembershipServiceBean = UpdateMembershipServiceBean.class.getInterfaces()[0];
        return updateMembershipServiceBean.getMethod("updateMembership", UpdateMembership.class);
    }

    private Method createEnableAutoRenewalMethod() throws NoSuchMethodException {
        Class<?> updateMembershipServiceBean = UpdateMembershipServiceBean.class.getInterfaces()[0];
        return updateMembershipServiceBean.getMethod("enableAutoRenewal", UpdateMembership.class);
    }

    private Method createReactivateStatusMethod() throws NoSuchMethodException {
        Class<?> updateMembershipServiceBean = UpdateMembershipServiceBean.class.getInterfaces()[0];
        return updateMembershipServiceBean.getMethod("reactivateMembership", UpdateMembership.class);
    }

    private Method createDisableAutoRenewalMethod() throws NoSuchMethodException {
        Class<?> updateMembershipServiceBean = UpdateMembershipServiceBean.class.getInterfaces()[0];
        return updateMembershipServiceBean.getMethod("disableAutoRenewal", UpdateMembership.class);
    }

    private Method createDeactivateStatusMethod() throws NoSuchMethodException {
        Class<?> updateMembershipServiceBean = UpdateMembershipServiceBean.class.getInterfaces()[0];
        return updateMembershipServiceBean.getMethod("deactivateMembership", UpdateMembership.class);
    }

    private void mockHttpServletRequest() {
        doReturn(Optional.of(VISIT_INFORMATION_HEADER_CONTENT)).when(requestContext).getHeader(VISIT_INFORMATION_HEADER);
        doReturn(Optional.of(MODULE_INFO_HEADER_CONTENT)).when(requestContext).getHeader(MODULE_INFO_HEADER);
    }

    private void mockEmptyHttpServletRequest() {
        doReturn(Optional.empty()).when(requestContext).getHeader(VISIT_INFORMATION_HEADER);
        doReturn(Optional.empty()).when(requestContext).getHeader(MODULE_INFO_HEADER);
    }
}
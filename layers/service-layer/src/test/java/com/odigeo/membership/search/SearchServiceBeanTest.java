package com.odigeo.membership.search;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.member.MemberAccountStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.parameters.search.MembershipSearchBuilder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class SearchServiceBeanTest {
    private static final List<MemberAccount> MEMBER_ACCOUNTS = Collections.singletonList(new MemberAccount(123L, 321L, "name", "lastName"));
    private static final List<Membership> MEMBERSHIPS = Collections.singletonList(new MembershipBuilder().build());
    private static final MembershipSearch MEMBERSHIP_SEARCH = new MembershipSearchBuilder().build();
    private static final MemberAccountSearch MEMBER_ACCOUNT_SEARCH = new MemberAccountSearch.Builder().build();
    private static final Long USER_ID_TEST = 123L;
    private static final String WEBSITE_TEST = "ES";
    private static final Long TEST_MEMBERSHIP_ID = 1234L;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private MembershipStore membershipStore;
    @InjectMocks
    private SearchServiceBean searchServiceBean = new SearchServiceBean();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }


    @Test
    public void testSearchMemberAccounts() throws SQLException, DataAccessException {
        when(memberAccountStore.searchMemberAccounts(any(DataSource.class), any(MemberAccountSearch.class))).thenReturn(MEMBER_ACCOUNTS);
        List<MemberAccount> memberAccounts = searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH);
        assertEquals(memberAccounts, MEMBER_ACCOUNTS);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchMemberAccountsException() throws SQLException, DataAccessException {
        when(memberAccountStore.searchMemberAccounts(any(DataSource.class), any(MemberAccountSearch.class))).thenThrow(new SQLException());
        searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH);
    }

    @Test
    public void testSearchMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class))).thenReturn(MEMBERSHIPS);
        List<Membership> memberships = searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH);
        assertEquals(memberships, MEMBERSHIPS);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testSearchMembershipException() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class))).thenThrow(new SQLException());
        searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH);
    }

    @Test
    public void testGetCurrentActivatedMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.ACTIVATED));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore).searchMemberships(any(), any());
        assertEquals(currentMembership.get().getStatus().name(), MemberStatus.ACTIVATED.name());
    }

    @Test
    public void testGetCurrentPendingToCollectMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.PENDING_TO_COLLECT));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore, times(2)).searchMemberships(any(), any());
        assertEquals(currentMembership.get().getStatus().name(), MemberStatus.PENDING_TO_COLLECT.name());
    }

    @Test
    public void testNoneCurrentCollectMembership() throws SQLException, DataAccessException {
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class)))
                .thenAnswer(invocation -> setCurrentMembershipSearchByStatus(invocation, MemberStatus.PENDING_TO_ACTIVATE));

        Optional<Membership> currentMembership = searchServiceBean.getCurrentMembership(USER_ID_TEST, WEBSITE_TEST);

        verify(membershipStore, times(2)).searchMemberships(any(), any());
        assertFalse(currentMembership.isPresent());
    }

    @Test
    public void checkLazyConfigurationEngineCalls() throws SQLException, DataAccessException {
        ConfigurationEngine.init(this::configure);
        SearchServiceBean searchServiceBean = new SearchServiceBean();
        when(membershipStore.searchMemberships(any(DataSource.class), any(MembershipSearch.class))).thenReturn(MEMBERSHIPS);
        when(memberAccountStore.searchMemberAccounts(any(DataSource.class), any(MemberAccountSearch.class))).thenReturn(MEMBER_ACCOUNTS);
        assertFalse(searchServiceBean.searchMemberAccounts(MEMBER_ACCOUNT_SEARCH).isEmpty());
        assertFalse(searchServiceBean.searchMemberships(MEMBERSHIP_SEARCH).isEmpty());
    }

    private List<com.odigeo.membership.Membership> setCurrentMembershipSearchByStatus(InvocationOnMock invocation, MemberStatus memberStatus) {
        MembershipSearch membershipSearch = (MembershipSearch) invocation.getArguments()[1];
        return Optional.of(membershipSearch.getValues())
                .filter(membershipSearch1 -> membershipSearch1.contains(memberStatus.name()))
                .map(valueList -> Arrays.asList(new MembershipBuilder().setId(TEST_MEMBERSHIP_ID).setStatus(memberStatus).build()))
                .orElse(Collections.emptyList());
    }

    private void configure(Binder binder) {
        binder.bind(MembershipStore.class).toInstance(membershipStore);
        binder.bind(MemberAccountStore.class).toInstance(memberAccountStore);
    }
}

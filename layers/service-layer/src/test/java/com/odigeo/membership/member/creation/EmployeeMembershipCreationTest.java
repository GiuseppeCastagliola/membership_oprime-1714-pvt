package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.search.SearchService;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EmployeeMembershipCreationTest {

    private static final String WEBSITE = "ES";
    private static final long USER_ID = 123L;
    private static final String NAME = "jo";
    private static final String LAST_NAMES = "ko";
    private static final long MEMBERSHIP_ID = 111L;
    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private SearchService searchService;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    private MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
            .withWebsite(WEBSITE)
            .withMemberAccountCreationBuilder(new MemberAccountCreation.MemberAccountCreationBuilder()
                    .userId(USER_ID)
                    .name(NAME)
                    .lastNames(LAST_NAMES))
            .withMembershipType(MembershipType.EMPLOYEE)
            .withMemberStatus(MemberStatus.ACTIVATED);
    private MembershipCreationFactory membershipCreationFactory;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
                .getInstance(membershipCreationBuilder.build());
    }

    private void configure(Binder binder) {
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(SearchService.class).toInstance(searchService);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManager);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof EmployeeMembershipCreation);
    }

    @Test
    public void employeeMembershipCreation() throws MissingElementException, DataAccessException, SQLException {
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        long membership = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        assertEquals(membership, MEMBERSHIP_ID);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(eq(MEMBERSHIP_ID));
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), eq(MEMBERSHIP_ID), eq(StatusAction.INTERNAL_CREATION));
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void employeeMembershipCreationAlreadyExists() throws MissingElementException, DataAccessException {
        MembershipCreation membershipCreationUserId = new MembershipCreation(membershipCreationBuilder.withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID)));
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(new MembershipBuilder().build()));
        membershipCreationFactory.createMembership(dataSource, membershipCreationUserId);
    }
}


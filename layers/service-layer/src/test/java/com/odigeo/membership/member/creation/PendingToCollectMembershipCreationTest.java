package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.fees.MembershipFeesService;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.product.MembershipSubscriptionFeeStore;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.product.v2.model.enums.ProductType;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PendingToCollectMembershipCreationTest {

    private static final long MEMBERSHIP_ID1 = 8L;
    private static final long MEMBERSHIP_ID2 = 16L;
    private static final long MEMBER_ACCOUNT_ID = 123L;
    private static final long USER_ID = 343L;
    private static final String WEBSITE_ES = "ES";
    private static final String WEBSITE_FR = "FR";
    private static final String NAME = "test";
    private static final String LAST_NAME = "test";
    private static final String MONTHS_DURATION_12 = "12";
    private static final LocalDateTime DATE_TODAY = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = DATE_TODAY.plusYears(1);
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final String CURRENCY = "EUR";
    private static final String RECURRING_ID = "abc";
    private static final SQLException SQL_EXCEPTION = new SQLException();
    private static final Long FEE_CONTAINER_ID = 22L;

    private MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
            .withMemberStatus(MemberStatus.PENDING_TO_COLLECT)
            .withMemberAccountId(MEMBER_ACCOUNT_ID)
            .withWebsite(WEBSITE_ES)
            .withSubscriptionPrice(SUBSCRIPTION_PRICE)
            .withCurrencyCode(CURRENCY)
            .withRecurringId(RECURRING_ID)
            .withMonthsDuration(MONTHS_DURATION_12)
            .withExpirationDate(EXPIRATION_DATE);
    private MembershipCreation membershipCreation = new MembershipCreation(membershipCreationBuilder);
    private MemberAccount memberAccountWithoutPendingToCollect = buildSimpleMemberAccount();

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MembershipRecurringStore membershipRecurringStore;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MembershipSubscriptionFeeStore membershipSubscriptionFeeStore;
    @Mock
    private MembershipFeesService membershipFeesService;

    private MembershipCreationFactory membershipCreationFactory;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
            .getInstance(membershipCreation);
    }

    private void configure(Binder binder) {
        binder.bind(MembershipStore.class).toInstance(membershipStore);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
        binder.bind(MembershipRecurringStore.class).toInstance(membershipRecurringStore);
        binder.bind(MembershipSubscriptionFeeStore.class).toInstance(membershipSubscriptionFeeStore);
        binder.bind(MemberAccountService.class).toInstance(memberAccountService);
        binder.bind(MembershipFeesService.class).toInstance(membershipFeesService);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof PendingToCollectMembershipCreation);
    }

    @Test
    public void testCreateMembership() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID1);
        when(membershipSubscriptionFeeStore.createMembershipFee(dataSource, String.valueOf(MEMBERSHIP_ID1),
            SUBSCRIPTION_PRICE, CURRENCY, ProductType.MEMBERSHIP_RENEWAL.toString())).thenReturn(Boolean.TRUE);
        when(memberAccountService.getMemberAccountById(membershipCreation.getMemberAccountId(), true))
                .thenReturn(memberAccountWithoutPendingToCollect);
        when(membershipFeesService.requestFeeContainerCreation(any(), any()))
                .thenReturn(Optional.of(FEE_CONTAINER_ID));
        long newMembershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        assertEquals(MEMBERSHIP_ID1, newMembershipId);
        verifyAllCallsToCreatePendingToCollect();
    }

    private void verifyAllCallsToCreatePendingToCollect() throws SQLException {
        verify(membershipStore).createMember(dataSource, membershipCreation);
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, MEMBERSHIP_ID1, StatusAction.CREATION);
        verify(membershipRecurringStore).insertMembershipRecurring(dataSource, MEMBERSHIP_ID1, RECURRING_ID);
        verify(membershipSubscriptionFeeStore).createMembershipFee(dataSource, String.valueOf(MEMBERSHIP_ID1),
            SUBSCRIPTION_PRICE, CURRENCY, ProductType.MEMBERSHIP_RENEWAL.toString());
    }

    @Test
    public void testCreateMembershipPendingToCollectAlreadyExistsOneForWebsite()
        throws MissingElementException, DataAccessException, SQLException {
        when(memberAccountService.getMemberAccountById(membershipCreation.getMemberAccountId(), true))
                .thenReturn(buildMemberAccountWithPendingToCollect(WEBSITE_ES));
        long newMembershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        assertEquals(MEMBERSHIP_ID2, newMembershipId);
        verify(membershipStore, never()).createMember(dataSource, membershipCreation);
    }

    @Test
    public void testCreateMembershipPendingToCollectExistsOneWithDifferentWebsite()
        throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID1);
        when(memberAccountService.getMemberAccountById(membershipCreation.getMemberAccountId(), true))
                .thenReturn(buildMemberAccountWithPendingToCollect(WEBSITE_FR));
        when(membershipFeesService.requestFeeContainerCreation(any(), any()))
                .thenReturn(Optional.of(FEE_CONTAINER_ID));
        long newMembershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        assertEquals(MEMBERSHIP_ID1, newMembershipId);
        verifyAllCallsToCreatePendingToCollect();
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testCreateMembershipWithExceptionInsertingFee()
        throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID1);
        doThrow(SQL_EXCEPTION).when(membershipSubscriptionFeeStore).createMembershipFee(dataSource, String.valueOf(
            MEMBERSHIP_ID1),
            SUBSCRIPTION_PRICE, CURRENCY, ProductType.MEMBERSHIP_RENEWAL.toString());
        when(memberAccountService.getMemberAccountById(membershipCreation.getMemberAccountId(), true))
                .thenReturn(memberAccountWithoutPendingToCollect);
        when(membershipFeesService.requestFeeContainerCreation(any(), any()))
                .thenReturn(Optional.of(FEE_CONTAINER_ID));
        membershipCreationFactory.createMembership(dataSource, membershipCreation);
    }

    private MemberAccount buildSimpleMemberAccount() {
        MemberAccount membershipAccount = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME);
        Membership membershipExpired = new MembershipBuilder().setId(MEMBERSHIP_ID1)
                .setStatus(MemberStatus.EXPIRED)
                .setExpirationDate(DATE_TODAY)
                .build();
        membershipAccount.setMemberships(Collections.singletonList(membershipExpired));
        return membershipAccount;
    }

    private MemberAccount buildMemberAccountWithPendingToCollect(String websiteForPendingToCollectMember) {
        MemberAccount membershipAccount = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME);
        Membership membershipExpired = new MembershipBuilder().setId(MEMBERSHIP_ID1)
            .setStatus(MemberStatus.EXPIRED).build();
        Membership membershipPendingToCollect = new MembershipBuilder().setId(MEMBERSHIP_ID2)
            .setStatus(MemberStatus.PENDING_TO_COLLECT).setWebsite(websiteForPendingToCollectMember).build();
        membershipAccount.setMemberships(Arrays.asList(membershipExpired, membershipPendingToCollect));
        return membershipAccount;
    }
}

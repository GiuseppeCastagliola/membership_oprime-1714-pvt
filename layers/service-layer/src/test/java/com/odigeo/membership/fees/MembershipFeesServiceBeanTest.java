package com.odigeo.membership.fees;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.fees.exception.FeesServiceException;
import com.odigeo.fees.model.FeeContainer;
import com.odigeo.fees.rest.FeesService;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.util.FeeMapper;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MembershipFeesServiceBeanTest {

    private static final String CURRENCY_CODE = "USD";
    private static final BigDecimal SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final Long FEE_CONTAINER_ID = 222L;

    @Mock
    FeesService feesService;
    @Mock
    FeeMapper feeMapper;

    MembershipFeesServiceBean membershipFeesServiceBean;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        membershipFeesServiceBean = new MembershipFeesServiceBean();
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(FeesService.class).toInstance(feesService);
        binder.bind(FeeMapper.class).toInstance(feeMapper);
    }

    @Test
    public void testRequestFeeContainerCreation() throws FeesServiceException {
        when(feesService.createContainer(any())).thenReturn(mockFeeContainer());
        Optional<Long> feeContainerId = membershipFeesServiceBean.requestFeeContainerCreation(SUBSCRIPTION_PRICE, CURRENCY_CODE);
        assertTrue(feeContainerId.isPresent());
        assertEquals(feeContainerId.get(), FEE_CONTAINER_ID);
    }

    @Test
    public void testRetrieveFees() throws FeesServiceException {
        when(feesService.findFeeContainer(anyLong())).thenReturn(mockFeeContainer());
        when(feeMapper.abstractFeeToProductFee(any())).thenCallRealMethod();
        when(feeMapper.getPrice(any(), any())).thenCallRealMethod();
        List<Fee> fees = membershipFeesServiceBean.retrieveFees(FEE_CONTAINER_ID);
        assertEquals(fees.size(), 1);
        assertEquals(fees.get(0).getPrice().getAmount(), SUBSCRIPTION_PRICE);
        assertEquals(fees.get(0).getPrice().getCurrency(), Currency.getInstance(CURRENCY_CODE));
    }

    @Test
    public void testFillProductFee() {
        when(feeMapper.getPrice(any(), any())).thenCallRealMethod();
        Fee fee = membershipFeesServiceBean.fillProductFee(SUBSCRIPTION_PRICE, CURRENCY_CODE);
        Price price = new FeeMapper().getPrice(SUBSCRIPTION_PRICE, CURRENCY_CODE);
        assertEquals(fee.getPrice(), price);
    }

    private FeeContainer mockFeeContainer() {
        FeeContainer feeContainer = new FeeContainer();
        feeContainer.setId(FEE_CONTAINER_ID);
        feeContainer.setFeeMap(new FeeMapper().membershipFeeToFeeLabelMap(SUBSCRIPTION_PRICE, CURRENCY_CODE));
        return feeContainer;
    }
}

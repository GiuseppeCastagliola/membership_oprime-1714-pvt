package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.MessageBodyWriterContext;
import org.jboss.resteasy.spi.interception.MessageBodyWriterInterceptor;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Provider
@ServerInterceptor
public class LoggingInterceptor implements PreProcessInterceptor, MessageBodyWriterInterceptor {

    protected static final Logger logger = Logger.getLogger(LoggingInterceptor.class);

    private final InterceptorHelper interceptorHelper = ConfigurationEngine.getInstance(InterceptorHelper.class);

    @Override
    public ServerResponse preProcess(HttpRequest request, ResourceMethod resourceMethod) {
        try {
            byte[] content = IOUtils.toByteArray(request.getInputStream());
            String preprocessedPath = request.getPreprocessedPath();
            if (!preprocessedPath.contains("auth")) {
                logger.info("Received request (" + preprocessedPath + "): " + interceptorHelper.buildOneLineString(content));
                MultivaluedMap<String, String> queryParameters = request.getUri().getQueryParameters();
                if (!queryParameters.isEmpty()) {
                    logger.info("queryParameters: " + ArrayUtils.toString(queryParameters.values()));
                }
            }
            request.setInputStream(new ByteArrayInputStream(content));
        } catch (IOException e) {
            throw new WebApplicationException(e);
        }
        return null;
    }

    @Override
    public void write(MessageBodyWriterContext context) throws IOException {
        OutputStream originalStream = context.getOutputStream();
        ByteArrayOutputStream temporaryStream = new ByteArrayOutputStream();
        writeStream(context, originalStream, temporaryStream);
    }

    private void writeStream(MessageBodyWriterContext context, OutputStream originalStream, ByteArrayOutputStream temporaryStream) throws IOException {
        context.setOutputStream(temporaryStream);
        context.proceed();
        logger.info("Sending Response: " + interceptorHelper.buildOneLineString(temporaryStream.toByteArray()));
        temporaryStream.writeTo(originalStream);
        temporaryStream.close();
        context.setOutputStream(originalStream);
    }

}

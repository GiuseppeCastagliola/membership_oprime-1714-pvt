package com.odigeo.membership.member.auth.exception;

public class MembershipExceptionBean {
    private String type;
    private String message;

    public MembershipExceptionBean(Class<?> exceptionClass, String message) {
        this.type = exceptionClass.getSimpleName();
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.util.StringUtils;

import javax.ws.rs.Path;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

@Singleton
public class InterceptorHelper {
    private static final String PATH_SEGMENT_DIVIDER = "/";

    private final StringUtils stringUtils = ConfigurationEngine.getInstance(StringUtils.class);

    String buildOneLineString(final byte[] content) {
        return stringUtils.replaceNewLineOrTabByWhiteSpace(new String(content, StandardCharsets.UTF_8));
    }

    boolean isRequestOf(Method method, String... endpoints) {
        Path path = method.getAnnotation(Path.class);
        return Arrays.stream(endpoints)
                .anyMatch(endpoint -> Objects.nonNull(path) && path.value().contains(PATH_SEGMENT_DIVIDER + endpoint));
    }

}

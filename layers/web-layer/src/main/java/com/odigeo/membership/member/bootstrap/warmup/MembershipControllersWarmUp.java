package com.odigeo.membership.member.bootstrap.warmup;

import com.google.inject.Inject;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.mapper.GeneralMapperCreator;
import com.odigeo.membership.member.memberapi.v1.CrmDecryptionController;
import com.odigeo.membership.member.memberapi.v1.PostBookingMemberServiceController;
import com.odigeo.membership.member.memberapi.v1.MemberUserAreaController;
import com.odigeo.membership.member.memberapi.v1.MembershipServiceController;
import com.odigeo.membership.member.memberapi.v1.SearchApiController;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.container.TravellerParametersContainer;
import com.odigeo.membership.request.postbooking.PostBookingPageInfoRequest;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import ma.glasnost.orika.MapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerCreditCardMapper;
import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerCreditCardTypeMapper;
import static com.odigeo.membership.member.rest.utils.CustomMapperRegistration.registerMemberSubscriptionDetailsMapper;

public class MembershipControllersWarmUp {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipControllersWarmUp.class);
    private static final String TEST_TOKEN = "9c8p1DkUAJuw2G83JI5ypjADygiPyw3RC%2FKHFPwhiQHveb6%2Fo1hXivaTObmJUoWJ"; //test token from OPRIME-651
    private static final long TEST_MEMBERSHIP_ID = 30L;
    private static final long TEST_MEMBER_ACCOUNT_ID = 1912L;
    private static final long TEST_USER_ID = 23335199L;
    private static final String EXPECTED_MEMBERSHIP_NOT_FOUND_EXCEPTION = "expected warmup MembershipNotFoundException caught for ";
    private final MemberUserAreaController memberUserAreaController = initializeMemberUserAreaController();
    private final PostBookingMemberServiceController postBookingMemberServiceController = initializePostBookingMemberServiceController();

    private final MembershipServiceController membershipServiceController;
    private final SearchApiController searchApiController;
    private final CrmDecryptionController crmDecryptionController;


    @Inject
    public MembershipControllersWarmUp(MembershipServiceController membershipServiceController,
                                       SearchApiController searchApiController,
                                       CrmDecryptionController crmDecryptionController) {
        this.membershipServiceController = membershipServiceController;
        this.searchApiController = searchApiController;
        this.crmDecryptionController = crmDecryptionController;
    }

    private static MemberUserAreaController initializeMemberUserAreaController() {
        MapperFactory mapperFactory = new GeneralMapperCreator().getMapperFactory();
        registerCreditCardMapper(mapperFactory);
        registerMemberSubscriptionDetailsMapper(mapperFactory);
        registerCreditCardTypeMapper(mapperFactory);

        return new MemberUserAreaController(mapperFactory.getMapperFacade());
    }

    private static PostBookingMemberServiceController initializePostBookingMemberServiceController() {
        return new PostBookingMemberServiceController(new GeneralMapperCreator().getMapper());
    }

    public void warmUp() {
        LOGGER.info("Warming up MemberUserAreaServiceBean");

        try {
            memberUserAreaController.getMembershipInfo(TEST_MEMBERSHIP_ID, false);
        } catch (MembershipNotFoundException e) {
            LOGGER.info(EXPECTED_MEMBERSHIP_NOT_FOUND_EXCEPTION + "memberUserAreaController.getMembershipInfo()");
        }

        try {
            searchApiController.getMembership(TEST_MEMBERSHIP_ID, false);
        } catch (MembershipNotFoundException e) {
            LOGGER.info(EXPECTED_MEMBERSHIP_NOT_FOUND_EXCEPTION + "searchApiController.getMembership");
        }

        try {
            searchApiController.getMemberAccount(TEST_MEMBER_ACCOUNT_ID, false);
        } catch (MembershipNotFoundException e) {
            LOGGER.info(EXPECTED_MEMBERSHIP_NOT_FOUND_EXCEPTION + "searchApiController.getMemberAccount");
        }

        searchApiController.searchAccounts(getMemberAccountSearchRequest());
        membershipServiceController.isMembershipPerksActiveOn("FR");
        membershipServiceController.applyMembership(getCheckMemberOnPassengerListRequest());
        membershipServiceController.getBlacklistedPaymentMethods(TEST_MEMBERSHIP_ID);
        postBookingMemberServiceController.getPostBookingPageInfo(getPostBookingPageInfoRequest());
        crmDecryptionController.getCrmDecryption(TEST_TOKEN);

       /*
        * MembershipPropertiesConfigController, PropertiesConfigServiceBean, PropertiesConfigurationManager, PropertiesCacheService,
        * PropertiesCacheConfiguration can not be safely warmed up without adding new getters to MembershipPropertiesConfigController
        * to call PropertiesCacheService.is...Active for each property and would need the appropriate PropertiesCacheConfiguration
        * config file for the environment
        */
    }

    private PostBookingPageInfoRequest getPostBookingPageInfoRequest() {
        PostBookingPageInfoRequest request = new PostBookingPageInfoRequest();
        request.setToken(TEST_TOKEN);
        return request;
    }

    private MemberAccountSearchRequest getMemberAccountSearchRequest() {
        return new MemberAccountSearchRequest.Builder()
                .userId(TEST_USER_ID)
                .withMemberships(false)
                .build();
    }

    private CheckMemberOnPassengerListRequest getCheckMemberOnPassengerListRequest() {
        TravellerParametersContainer travellerParametersContainer = new TravellerParametersContainer();
        travellerParametersContainer.setName("WarmUp");
        travellerParametersContainer.setLastNames("WarmUp");

        CheckMemberOnPassengerListRequest request = new CheckMemberOnPassengerListRequest();
        request.setSite("FR");
        request.setUserId(TEST_USER_ID);
        request.setTravellerContainerList(Collections.singletonList(travellerParametersContainer));

        return request;
    }
}

package com.odigeo.membership.member.bootstrap;

import com.odigeo.bookingapi.v14.BookingApiService;
import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.member.rest.ServiceConfigurationBuilder;

public class BookingApiServiceModule extends AbstractRestUtilsModule {

    public BookingApiServiceModule(ServiceNotificator... serviceNotificators) {
        super(BookingApiService.class, serviceNotificators);
    }

    @Override
    protected ServiceConfiguration getServiceConfiguration(Class aClass) {
        return ServiceConfigurationBuilder.setUpBookingApi(BookingApiService.class);
    }
}

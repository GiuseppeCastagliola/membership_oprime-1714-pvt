import React, {Component} from 'react';
import {Modal, Table} from 'antd';

class Instructions extends Component {
    state = { visible: false }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = () => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    render() {
        const columns = [
            {
                title: 'first_name',
                dataIndex: 'first_name',
                key: 'first_name',
            },
            {
                title: 'surname',
                dataIndex: 'surname',
                key: 'surname',
            },
            {
                title: 'website',
                key: 'website',
                dataIndex: 'website',
            },
            {
                title: 'email',
                key: 'email',
                dataIndex: 'email',
            }
        ];

        const data = [{
            key: '1',
            first_name: 'Manolo',
            surname: 'Garcia',
            website: 'ES',
            email: 'foo@gmail.com',
        }, {
            key: '2',
            first_name: 'Fleur',
            surname: 'Bisset',
            website: 'FR',
            email: 'bar@gmail.com',
        }];

        return (
            <div>
                <a type="primary" onClick={this.showModal}>
                    File structure
                </a>
                <Modal
                    title="File example"
                    width={810}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <Table columns={columns} dataSource={data} pagination={false}/>
                    <code>
                        first_name;surname;website;email<br/>
                        Manolo;Garcia;ES;foo@gmail.com<br/>
                        Fleur;Bisset;FR;bar@gmail.com
                    </code>
                    <br/>
                </Modal>
            </div>
        );
    }
}

export default Instructions;

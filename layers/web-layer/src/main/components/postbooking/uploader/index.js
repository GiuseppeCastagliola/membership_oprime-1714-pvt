import React, {Component} from 'react';
import Bem from 'react-bem-helper';
import {Col, Icon, message, Row, Upload} from 'antd';
import {Breadcrumb, Instructions} from 'Components';
import axios from 'axios';
import './style.scss';

const Dragger = Upload.Dragger;
const classes = new Bem('uploader');
const fileName = "postBookingFile";
const fileAccept = ".csv";
const fileAction = "/membership/membership/v1/post-booking/file-service";

class Uploader extends Component {
    state = {
        fileUploaded: false,
        currentStep: 0,
        statusStep: 'process',
        internalEmployees: true,
        channel: 'PHONE',
        sourceEnabled: true,
    };

    sendFile = ({file, action, onSuccess, onError}) => {
        const data = new FormData();
        const {
            internalEmployees,
            sourceEnabled,
            channel
        } = this.state;

        if (sourceEnabled) {
            data.append('channel', channel);
        }
        data.append('file', file);
        data.append('internalEmployees', internalEmployees);

        axios.post(action, data).then(({data}) => {
            const {errors} = data;
            if (Object.keys(errors).length !== 0) {
                this.handleErrors(errors);
                return onError();
            }
            return onSuccess();
        }).catch(({message: errorMessage}) => {
            onError();
            this.handleErrors([errorMessage]);
        });
    };

    handleErrors = (errors) => {
        let text = '';
        errors.forEach((error) => {
            text = `${text}${error}\n`;
        });
        message.error(text);
        this.downloadText(text);
    };

    downloadText = (text) => {
        let element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', 'postbookinResult');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    };

    onChangeFile = ({file}) => {
        switch (file.status) {
            case 'uploading':
                this.setStep(1);
                this.setState({'fileUploaded': true});
                break;
            case 'done':
                this.setStep(2);
                this.setState({'fileUploaded': false});
                break;
            case 'removed':
                this.setStep(0);
                this.setState({'fileUploaded': false});
                break;
            case 'error':
                this.setStep(1, 'error');
                this.setState({'fileUploaded': false});
                break;
            default:
        }
    };

    setStep = (step, status = 'process') => {
        this.setState({'currentStep': step});
        this.setState({'statusStep': status});
    };

    render() {
        return (
            <div>
                <Row>
                    <Col span={24}>
                        <Breadcrumb current={this.state.currentStep} status={this.state.statusStep}/>
                    </Col>
                </Row>
                <Row>
                    <Col span={12} offset={6}>
                        <Dragger
                            {...
                                classes('dragger')
                            }
                            name={fileName}
                            accept={fileAccept}
                            action={fileAction}
                            customRequest={this.sendFile}
                            onChange={this.onChangeFile}
                            disabled={this.state.fileUploaded}>
                            <p className="ant-upload-drag-icon">
                                <Icon type="inbox"/>
                            </p>
                            <p className="ant-upload-text">
                                Click or drag file to this area to upload
                            </p>
                            <p className="ant-upload-hint">
                                *Note: The only available file extension is .csv. Read the .csv file structure guideline
                                before start.
                            </p>
                        </Dragger>
                        <Row gutter={100} justify="center" {...classes('controls')}>
                            <Col span={10}>
                            </Col>
                            <Col span={6} offset={8}>
                                <Instructions/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Uploader;

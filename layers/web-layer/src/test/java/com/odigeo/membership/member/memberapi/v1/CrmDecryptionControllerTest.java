package com.odigeo.membership.member.memberapi.v1;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.util.CrmCipher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class CrmDecryptionControllerTest {

    private static final String BLANK_STRING = " ";
    private static final String INVALID_TOKEN = "INVALID_TOKEN";
    private static final String VALID_STRING = "VALID_STRING";
    private static final String VALID_RESULT = "VALID_RESULT";
    private static final String EXCEPTION_MESSAGE = "EXCEPTION_MESSAGE";

    @Mock
    private CrmCipher cipherMock;

    @InjectMocks
    private CrmDecryptionController crmDecryptionController = new CrmDecryptionController();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(CrmCipher.class).toInstance(cipherMock));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testGetCrmDecryptionNullToken() throws UnsupportedEncodingException {
        when(cipherMock.decryptToken(null)).thenThrow(new NullPointerException());

        crmDecryptionController.getCrmDecryption(null);
    }

    @Test
    public void testGetCrmDecryptionBlankToken() throws UnsupportedEncodingException {
        when(cipherMock.decryptToken(BLANK_STRING)).thenReturn(BLANK_STRING);

        assertEquals(BLANK_STRING, crmDecryptionController.getCrmDecryption(BLANK_STRING));
    }

    @Test(expectedExceptions = MembershipServiceException.class, expectedExceptionsMessageRegExp = EXCEPTION_MESSAGE)
    public void testGetCrmDecryptionInvalidToken() throws UnsupportedEncodingException {
        when(cipherMock.decryptToken(INVALID_TOKEN)).thenThrow(new UnsupportedEncodingException(EXCEPTION_MESSAGE));

        crmDecryptionController.getCrmDecryption(INVALID_TOKEN);
    }

    @Test
    public void testGetCrmDecryptionCorrectToken() throws UnsupportedEncodingException {
        when(cipherMock.decryptToken(VALID_STRING)).thenReturn(VALID_RESULT);

        assertEquals(VALID_RESULT, crmDecryptionController.getCrmDecryption(VALID_STRING));
    }
}

package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.memberapi.v1.MembershipServiceController;
import com.odigeo.membership.util.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class InterceptorHelperTest {
    private static String ENABLE_AUTORENEWAL_MEMBERSHIP_URI = "http://localhost/membership/membership/v1/disable-auto-renewal/22";
    private static String EXAMPLE_MEMBERSHIP_URI = "http://localhost/membership/membership/v1/a/b/c";

    private InterceptorHelper interceptorHelper;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(StringUtils.class).toInstance(new StringUtils()));
        interceptorHelper = new InterceptorHelper();
    }

    @Test
    public void testBuildOneLineString() {
        String line = interceptorHelper.buildOneLineString("\t A \nB \t C \n".getBytes());
        assertEquals(line, "  A  B   C  ");
    }

    @Test
    public void testIsRequestOf() throws NoSuchMethodException {
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("disableAutoRenewal", Long.class);
        assertTrue(interceptorHelper.isRequestOf(disableAutoRenewalMethod, "disable-auto-renewal/{id}"));
    }
}
package com.odigeo.membership.member.rest.exception.mapper;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

public class MembershipServiceExceptionMapperTest {

    @Test
    public void testMembershipServiceExceptionMapper() {
        MembershipServiceExceptionMapper mapper = new MembershipServiceExceptionMapper();
        MembershipInternalServerErrorException membershipInternalServerErrorException = new MembershipInternalServerErrorException(StringUtils.EMPTY);
        Response response = mapper.toResponse(membershipInternalServerErrorException);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }
}

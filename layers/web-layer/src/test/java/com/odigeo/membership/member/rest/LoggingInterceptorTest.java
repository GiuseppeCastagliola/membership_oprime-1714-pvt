package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.MessageBodyWriterContext;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.fail;

public class LoggingInterceptorTest {

    @Mock
    private LoggingInterceptor loggingInterceptorMock;

    @Mock
    private HttpRequest httpRequestMock;

    @Mock
    private ResourceMethod resourceMethodMock;

    @Mock
    private InputStream inputStreamMock;
    private LoggingInterceptor loggingInterceptor;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        this.loggingInterceptorMock = mock(LoggingInterceptor.class);
        ConfigurationEngine.init(new AbstractModule() {
            protected void configure() {
                bind(LoggingInterceptor.class).toInstance(loggingInterceptorMock);
                bind(HttpRequest.class).toInstance(httpRequestMock);
                bind(ResourceMethod.class).toInstance(resourceMethodMock);
                bind(InputStream.class).toProvider(() -> inputStreamMock);
            }
        });
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testWriteThrowsNpeIfInterceptorDidNotInitProperly() throws Exception {
        final MessageBodyWriterContext messageBodyWriterContext = mock(MessageBodyWriterContext.class);
        doCallRealMethod().when(loggingInterceptorMock).write(messageBodyWriterContext);
        this.loggingInterceptorMock.write(messageBodyWriterContext);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testPreProcessThrowsNpeIfInvalidInputStream() {
        new LoggingInterceptor().preProcess(httpRequestMock, resourceMethodMock);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testWriteThrowsNpeIfInvalidInputStream() {
        try {
            MessageBodyWriterContext messageBodyWriterContextMock = mock(MessageBodyWriterContext.class);
            loggingInterceptor = new LoggingInterceptor();
            loggingInterceptor.write(messageBodyWriterContextMock);
        } catch (final IOException e) {
            fail();
        }
    }
}

package com.odigeo.membership.member.bootstrap;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class BookingApiServiceModuleTest {

    private BookingApiServiceModule bookingApiServiceModule;

    @BeforeMethod
    public void setUp() throws Exception {
        this.bookingApiServiceModule = new BookingApiServiceModule();
    }

    @Test
    public void testGetServiceConfigurationReturnsNonNullInstanceIndependentlyOfServiceNotificators() {
        assertNotNull(this.bookingApiServiceModule.getServiceConfiguration(Object.class));
        assertNotNull(new BookingApiServiceModule(null));
    }
}

package com.odigeo.membership.mocks.feesapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class FeesServiceHttpServer extends JaxRsServiceHttpServer {

    private static final String PATH = "/fees-service/v1";
    private static final int PORT = 58001;

    @Inject
    public FeesServiceHttpServer() {
        super(PATH, PORT);
    }
}

package com.odigeo.membership.mocks.kafka.consumers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.mocks.kafka.processors.WelcomeToPrimeMessageProcessor;

@Singleton
public class WelcomeToPrimeMessageConsumer extends AbstractKafkaMessageConsumer<MembershipMailerMessage> {
    private static final String TOPIC_NAME = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1";
    private static final String GROUP_ID = "welcome-to-prime-group";
    private final WelcomeToPrimeMessageProcessor messageProcessor;

    @Inject
    public WelcomeToPrimeMessageConsumer(WelcomeToPrimeMessageProcessor welcomeToPrimeMessageProcessor) {
        super(welcomeToPrimeMessageProcessor, MembershipMailerMessage.class, TOPIC_NAME, GROUP_ID);
        this.messageProcessor = welcomeToPrimeMessageProcessor;
    }

    @Override
    public WelcomeToPrimeMessageProcessor getMessageProcessor() {
        return messageProcessor;
    }
}

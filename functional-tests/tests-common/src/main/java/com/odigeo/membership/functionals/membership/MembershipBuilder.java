package com.odigeo.membership.functionals.membership;

import java.math.BigDecimal;

public class MembershipBuilder {

    private Long memberId;
    private String website;
    private String status;
    private String autoRenewal;
    private Long memberAccountId;
    private String activationDate;
    private String expirationDate;
    private BigDecimal balance;
    private String productStatus;
    private String sourceType;
    private Long monthsDuration;
    private BigDecimal totalPrice;
    private String currencyCode;
    private String membershipType;
    private String timestamp;
    private String feeContainerId;

    public Long getMemberId() {
        return memberId;
    }

    public String getWebsite() {
        return website;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public Long getMonthsDuration() {
        return monthsDuration;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public MembershipBuilder setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public MembershipBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public MembershipBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public MembershipBuilder setAutoRenewal(String autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public MembershipBuilder setMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MembershipBuilder setActivationDate(String activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public MembershipBuilder setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public MembershipBuilder setProductStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public MembershipBuilder setMonthsDuration(Long monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public MembershipBuilder setSourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public MembershipBuilder setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public MembershipBuilder setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public MembershipBuilder setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getSourceType() {
        return sourceType;
    }

    public MembershipBuilder setMembershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public MembershipBuilder setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getFeeContainerId() {
        return feeContainerId;
    }

    public MembershipBuilder setFeeContainerId(String feeContainerId) {
        this.feeContainerId = feeContainerId;
        return this;
    }
}

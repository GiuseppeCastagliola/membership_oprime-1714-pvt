package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.MembershipBuilder;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class MembershipStore {
    private static final String SAVE_MEMBERSHIP = "INSERT INTO MEMBERSHIP_OWN.GE_MEMBERSHIP (ID, WEBSITE, STATUS, AUTO_RENEWAL, MEMBER_ACCOUNT_ID, ACTIVATION_DATE, EXPIRATION_DATE, BALANCE, PRODUCT_STATUS, SOURCE_TYPE, TOTAL_PRICE, CURRENCY_CODE, MONTHS_DURATION, MEMBERSHIP_TYPE, TIMESTAMP, FEE_CONTAINER_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String FIND_MEMBERSHIP_BALANCE = "SELECT BALANCE FROM MEMBERSHIP_OWN.GE_MEMBERSHIP WHERE ID = ?";
    private static final String GET_MEMBERSHIP_BY_ID_SQL = "SELECT ID, WEBSITE, STATUS, AUTO_RENEWAL, MEMBER_ACCOUNT_ID, ACTIVATION_DATE, EXPIRATION_DATE, BALANCE, PRODUCT_STATUS, SOURCE_TYPE, MEMBERSHIP_TYPE, MONTHS_DURATION, TOTAL_PRICE, CURRENCY_CODE, FEE_CONTAINER_ID FROM GE_MEMBERSHIP WHERE ID = ?";
    private static final String GET_MEMBERSHIP_BY_MEMBER_ACCOUNT_ID_SQL = "SELECT ID, WEBSITE, STATUS, AUTO_RENEWAL, MEMBER_ACCOUNT_ID, ACTIVATION_DATE, EXPIRATION_DATE, BALANCE, PRODUCT_STATUS, SOURCE_TYPE, MEMBERSHIP_TYPE, MONTHS_DURATION, TOTAL_PRICE, CURRENCY_CODE, FEE_CONTAINER_ID FROM GE_MEMBERSHIP WHERE MEMBER_ACCOUNT_ID = ?";
    private static final long DEFAULT_MONTH_DURATION = 12L;

    private static final Predicate<String> NULL_PREDICATE = string -> isNull(string) || "null".equalsIgnoreCase(string);

    public void saveMemberships(Connection conn, List<MembershipBuilder> memberships) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_MEMBERSHIP)) {
            for (MembershipBuilder membershipBuilder : memberships) {
                int idx = 1;
                pstmt.setLong(idx++, membershipBuilder.getMemberId());
                setStringOrNull(pstmt, membershipBuilder.getWebsite(), idx++);
                setStringOrNull(pstmt, membershipBuilder.getStatus(), idx++);
                setStringOrNull(pstmt, membershipBuilder.getAutoRenewal(), idx++);
                pstmt.setLong(idx++, membershipBuilder.getMemberAccountId());
                setDateOrNull(pstmt, membershipBuilder.getActivationDate(), idx++);
                setDateOrNull(pstmt, membershipBuilder.getExpirationDate(), idx++);
                pstmt.setBigDecimal(idx++, membershipBuilder.getBalance());
                setStringOrNull(pstmt, membershipBuilder.getProductStatus(), idx++);
                setStringOrNull(pstmt, membershipBuilder.getSourceType(), idx++);
                pstmt.setBigDecimal(idx++, membershipBuilder.getTotalPrice());
                setStringOrNull(pstmt, membershipBuilder.getCurrencyCode(), idx++);
                setMonthDuration(pstmt, membershipBuilder.getMonthsDuration(), idx++);
                pstmt.setString(idx++, membershipBuilder.getMembershipType());
                pstmt.setTimestamp(idx++, Timestamp.valueOf(LocalDateTime.parse(membershipBuilder.getTimestamp())));
                setLongOrNull(pstmt, membershipBuilder.getFeeContainerId(), idx);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

    public MembershipBuilder getMembershipById(Connection conn, long membershipId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_MEMBERSHIP_BY_ID_SQL)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return buildMembership(rs);
                }
            }
        }
        return null;
    }

    public List<MembershipBuilder> getMembershipByMemberAccountId(Connection conn, long memberAccountId) throws SQLException {
        List<MembershipBuilder> memberships = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_MEMBERSHIP_BY_MEMBER_ACCOUNT_ID_SQL)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    memberships.add(buildMembership(rs));
                }
            }
        }
        return memberships;
    }

    private MembershipBuilder buildMembership(ResultSet rs) throws SQLException {
        return new MembershipBuilder()
                .setMemberId(rs.getLong("ID"))
                .setWebsite(rs.getString("WEBSITE"))
                .setStatus(rs.getString("STATUS"))
                .setAutoRenewal(rs.getString("AUTO_RENEWAL"))
                .setMemberAccountId(rs.getLong("MEMBER_ACCOUNT_ID"))
                .setActivationDate(rs.getString("ACTIVATION_DATE"))
                .setExpirationDate(rs.getString("EXPIRATION_DATE"))
                .setBalance(rs.getBigDecimal("BALANCE"))
                .setProductStatus(rs.getString("PRODUCT_STATUS"))
                .setSourceType(rs.getString("SOURCE_TYPE"))
                .setMembershipType(rs.getString("MEMBERSHIP_TYPE"))
                .setMonthsDuration(rs.getLong("MONTHS_DURATION"))
                .setTotalPrice(rs.getBigDecimal("TOTAL_PRICE"))
                .setCurrencyCode(rs.getString("CURRENCY_CODE"))
                .setFeeContainerId(rs.getString("FEE_CONTAINER_ID"));
    }

    private void setLongOrNull(PreparedStatement pstmt, String string, int idx) throws SQLException {
        if (NULL_PREDICATE.test(string)) {
            pstmt.setNull(idx, Types.NUMERIC);
        } else {
            pstmt.setLong(idx, Long.parseLong(string));
        }
    }

    private void setStringOrNull(PreparedStatement pstmt, String string, int idx) throws SQLException {
        if (NULL_PREDICATE.test(string)) {
            pstmt.setNull(idx, Types.VARCHAR);
        } else {
            pstmt.setString(idx, string);
        }
    }

    private void setDateOrNull(PreparedStatement pstmt, String date, int idx) throws SQLException {
        if (NULL_PREDICATE.test(date)) {
            pstmt.setNull(idx, Types.DATE);
        } else {
            pstmt.setDate(idx, Date.valueOf(date));
        }
    }

    private void setMonthDuration(PreparedStatement pstmt, Long monthsDuration, int idx) throws SQLException {
        if (nonNull(monthsDuration)) {
            pstmt.setLong(idx, monthsDuration);
        } else {
            pstmt.setLong(idx, DEFAULT_MONTH_DURATION);
        }
    }

    public Optional<BigDecimal> findMembershipBalance(final Connection conn, final Long membershipId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(FIND_MEMBERSHIP_BALANCE)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return Optional.ofNullable(rs.getBigDecimal(1));
                }
                return Optional.empty();
            }
        }
    }
}

package com.odigeo.membership.functionals.membership;

import com.odigeo.commons.test.random.StringPicker;

import java.util.Random;

public class MembershipCreationRequestBuilder {

    private StringPicker stringPicker;

    private String userId;
    private String name;
    private String lastNames;
    private String website;
    private String monthsToRenewal;
    private String email;
    private String membershipType;

    public String getUserId() {
        return userId == null ? getStringPicker().pickAsciiNumeric(12) : userId;
    }

    public MembershipCreationRequestBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return name == null ? getRandomAlphanumericValue(12) : name;
    }

    public MembershipCreationRequestBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastNames() {
        return lastNames == null ? getRandomAlphanumericValue(12) : lastNames;
    }

    public MembershipCreationRequestBuilder setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public String getWebsite() {
        return website == null ? getRandomAlphanumericValue(2) : website;
    }

    public MembershipCreationRequestBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public String getMonthsToRenewal() {
        int randomMonths = new Random().ints(1, (24 + 1)).findFirst().getAsInt();
        return monthsToRenewal == null ? String.valueOf(randomMonths) : monthsToRenewal;
    }

    public MembershipCreationRequestBuilder setMonthsToRenewal(String monthsToRenewal) {
        this.monthsToRenewal = monthsToRenewal;
        return this;
    }

    public String getEmail() {
        return email == null ? getRandomAlphanumericValue(12) : email;
    }

    public MembershipCreationRequestBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getMembershipType() {
        return membershipType == null ? "BASIC" : membershipType;
    }

    public MembershipCreationRequestBuilder setMembershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    private StringPicker getStringPicker() {
        if (stringPicker == null) {
            stringPicker = new StringPicker(new Random());
        }
        return stringPicker;
    }

    private String getRandomAlphanumericValue(int length) {
        return getStringPicker().pickAsciiAlphanumeric(length);
    }
}



package com.odigeo.membership.functionals.config;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class KafkaConfig {
    private static final Integer PORT = 9092;
    private static final Integer ZOO_KEEPER_PORT = 2181;

    private String localIp;

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public Integer getPort() {
        return PORT;
    }

    public Integer getZooKeeperPort() {
        return ZOO_KEEPER_PORT;
    }
}

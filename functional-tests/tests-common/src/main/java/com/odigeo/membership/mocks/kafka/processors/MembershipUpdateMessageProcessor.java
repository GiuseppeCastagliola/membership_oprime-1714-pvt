package com.odigeo.membership.mocks.kafka.processors;

import com.google.inject.Singleton;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class MembershipUpdateMessageProcessor extends AbstractKafkaMessageProcessor<MembershipUpdateMessage> {
    private static final Logger LOGGER = Logger.getLogger(MembershipUpdateMessageProcessor.class);
    private final List<MembershipUpdateMessage> receivedMessages = new ArrayList<>();

    @Override
    public void onMessage(MembershipUpdateMessage message) {
        LOGGER.info("Received new MembershipUpdate message from kafka for membershipId: " + message.getMembershipId());
        receivedMessages.add(message);
    }

    public List<MembershipUpdateMessage> getReceivedMessages() {
        return receivedMessages;
    }

    public void resetMessageList() {
        receivedMessages.clear();
    }
}

package com.odigeo.membership.world;

import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

@ScenarioScoped
public class MembershipSearchManagementWorld {
    private List<MembershipResponse> membershipSearchResponse;
    private List<MemberAccountResponse> memberAccountResponses;

    public List<MembershipResponse> getMembershipSearchResponse() {
        return membershipSearchResponse;
    }

    public void setMembershipSearchResponse(List<MembershipResponse> membershipSearchResponse) {
        this.membershipSearchResponse = membershipSearchResponse;
    }

    public List<MemberAccountResponse> getMemberAccountResponses() {
        return memberAccountResponses;
    }

    public void setMemberAccountResponses(List<MemberAccountResponse> memberAccountResponses) {
        this.memberAccountResponses = memberAccountResponses;
    }
}

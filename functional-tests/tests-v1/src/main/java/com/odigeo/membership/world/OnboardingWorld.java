package com.odigeo.membership.world;

import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class OnboardingWorld {

    private OnboardingEventRequest createRequest;
    private Long createdOnboardingId;

    public OnboardingEventRequest getCreateRequest() {
        return createRequest;
    }

    public void setCreateRequest(OnboardingEventRequest createRequest) {
        this.createRequest = createRequest;
    }

    public Long getCreatedOnboardingId() {
        return createdOnboardingId;
    }

    public void setCreatedOnboardingId(Long createdOnboardingId) {
        this.createdOnboardingId = createdOnboardingId;
    }
}

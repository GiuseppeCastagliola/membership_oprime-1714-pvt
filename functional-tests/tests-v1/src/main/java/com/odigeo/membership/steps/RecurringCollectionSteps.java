package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.functionals.membership.RecurringCollectionVerifier;
import com.odigeo.membership.response.Membership;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

@ScenarioScoped
public class RecurringCollectionSteps extends CommonSteps {

    @Inject
    public RecurringCollectionSteps(ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
    }

    @Then("^the following membership recurring information is stored in db:$")
    public void verifyRecurringCollectionInformation(List<RecurringCollectionVerifier> verifierList) throws InterruptedException {
        verifierList.forEach(this::verifyRecurring);
    }

    private void verifyRecurring(RecurringCollectionVerifier recurringCollectionVerifier) {
        try {
            String userId = recurringCollectionVerifier.getUserId();
            Membership membership = membershipService.getMembership(Long.parseLong(userId), recurringCollectionVerifier.getWebsite());
            String actualRecurringId = membership.getRecurringId();
            String expectedRecurringId = recurringCollectionVerifier.getRecurringId();
            assertEquals(actualRecurringId, expectedRecurringId);
            Long actualMembershipId = membership.getMembershipId();
            Long expectedMembershipId = Long.parseLong(recurringCollectionVerifier.getMembershipId());
            assertEquals(actualMembershipId, expectedMembershipId);
            String actualWebsite = membership.getWebsite();
            String expectedWebsite = recurringCollectionVerifier.getWebsite();
            assertEquals(actualWebsite, expectedWebsite);
        } catch (InvalidParametersException e) {
            fail("Should not have thrown InvalidParametersException");
        }
    }
}

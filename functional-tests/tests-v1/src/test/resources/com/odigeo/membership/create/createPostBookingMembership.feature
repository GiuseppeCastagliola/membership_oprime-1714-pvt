Feature: Test createPostBookingMemberAccount service

  Scenario Outline: createPostBookingMember
    Given the next createMembershipSubscriptionRequest:
      | userId   | name   | lastNames   | website   | monthsToRenewal   | email   |
      | <userId> | <name> | <lastNames> | <website> | <monthsToRenewal> | <email> |
    And the user-api has the following user info
      | userId   | status       | email   | brand | token                    | tokenType        |
      | <userId> | <userStatus> | <email> | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When the client request createPostBookingMemberAccount to PostBooking API
    Then the post booking membership is created with this information and status ACTIVATED
    And membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to <email>
    And the message with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> is sent to kafka
    Examples:
      | userId | name | lastNames | website | monthsToRenewal | email                 | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  |
      | 333    | JUAN | PEREZ     | ES      |                 | juan.perez@gmail.com  | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA |
      | 334    | JOSE | PEREZ     | FR      | 3               | jose.perez@gmail.com  | ACTIVE        | passwordTokenB         | false             | null           |
      | 335    | JUAN | PEREZ     | IT      | 6               | juan.perez@tiscali.it | PENDING_LOGIN | null                   | false             | null           |

  Scenario Outline: createPostBookingMembershipPending
    Given the next createMembershipSubscriptionRequest:
      | userId   | name   | lastNames   | website   | monthsToRenewal   |
      | <userId> | <name> | <lastNames> | <website> | <monthsToRenewal> |
    When the client request createPostBookingPendingMembership to PostBooking API
    Then the post booking membership is created with this information and status PENDING_TO_ACTIVATE
    Examples:
      | userId | name | lastNames | website | monthsToRenewal |
      | 333    | JUAN | PEREZ     | ES      | 12              |
      | 334    | JOSE | PEREZ     | FR      | 3               |
      | 335    | JUAN | PEREZ     | IT      | 6               |

  Scenario Outline: createPostBookingMembership with the proper membershipType
    Given the next createMembershipSubscriptionRequest:
      | membershipType   |
      | <membershipType> |
    When the client request createPostBookingPendingMembership to PostBooking API
    Then a post booking membership is created with the proper membership type
    Examples:
      | membershipType |
      |                |
      | BASIC          |
      | BASIC_FREE     |
      | BUSINESS       |

  Scenario Outline: createPostBookingMembership stores source type of post booking
    Given the next createMembershipSubscriptionRequest:
      | userId   | name   | lastNames   | website   | monthsToRenewal   | email   |
      | <userId> | <name> | <lastNames> | <website> | <monthsToRenewal> | <email> |
    And the user-api has the following user info
      | userId   | status       | email   | brand | token                    | tokenType        |
      | <userId> | <userStatus> | <email> | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When the client request createPostBookingMemberAccount to PostBooking API
    Then a membership is created with source type POST_BOOKING
    And membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to <email>
    And the message with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> is sent to kafka
    Examples:
      | userId | name | lastNames | website | monthsToRenewal | email                | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  |
      | 333    | JUAN | PEREZ     | ES      |                 | juan.perez@gmail.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA |
      | 334    | JOSE | PEREZ     | FR      | 6               | juan.perez@gmail.com | ACTIVE        | passwordTokenB         | false             | null           |
      | 334    | JOSE | PEREZ     | FR      | 6               | juan.perez@gmail.com | PENDING_LOGIN | null                   | false             | null           |

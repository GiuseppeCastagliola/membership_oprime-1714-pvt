Feature: Test getMembership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 555             | 1555   | MARIO     | GOMEZ     |
      | 678             | 1999   | LUIS      | GONZALEZ  |
      | 399             | 3333   | SERGIO    | LOPEZ     |
      | 400             | 3333   | SERGIO    | LOPEZ     |
      | 700             | 7654   | FRANCISCO | PEREZ     |

    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 321      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 322      | ES          | EXPIRED             | DISABLED    | 321             | 2016-07-06     | 2017-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 323      | IT          | EXPIRED             | DISABLED    | 321             | 2016-07-06     | 2017-07-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 987      | ES          | EXPIRED             | ENABLED     | 555             | 2017-11-11     | 2018-11-11     | 54.99   | FUNNEL_BOOKING | 6              |
      | 989      | ES          | DEACTIVATED         | ENABLED     | 555             | 2016-11-11     | 2017-11-11     | 54.99   | FUNNEL_BOOKING | 12             |
      | 678      | ES          | ACTIVATED           | ENABLED     | 678             | 2018-08-06     | 2019-08-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 679      | ES          | EXPIRED             | ENABLED     | 678             | 2017-08-06     | 2018-08-06     | 54.99   | FUNNEL_BOOKING | 1              |
      | 380      | IT          | EXPIRED             | ENABLED     | 399             | 2017-08-06     | 2018-08-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 381      | IT          | ACTIVATED           | DISABLED    | 400             | 2018-08-06     | 2019-08-06     | 54.99   | FUNNEL_BOOKING | 6              |
      | 382      | FR          | ACTIVATED           | DISABLED    | 401             | 2018-09-06     | 2019-09-06     | 54.99   | FUNNEL_BOOKING | 12             |
      | 765      | ES          | ACTIVATED           | ENABLED     | 700             | 2019-06-21     | 2020-06-21     | 54.99   | POST_BOOKING   | 6              |

  Scenario Outline: Checking PERKS_USAGE_EXCEEDED warnings
    Given membership rule of max 5 bookings on 15 days and rule activation value <activated>
    And member <memberId> has <numBookings> bookings in the last <numDays> days
    When requested the member information with userId <userId> and website <website>
    Then the warningsList size is <listSize>

    Examples:
      | memberId | numBookings | numDays | listSize | userId | website | activated |
      | 123      | 3           | 15      | 0        | 4321   | ES      | 1         |
      | 123      | 9           | 15      | 1        | 4321   | ES      | 1         |
      | 123      | 4           | 15      | 0        | 4321   | ES      | 1         |
      | 678      | 2           | 15      | 0        | 1999   | ES      | 1         |
      | 381      | 4           | 10      | 0        | 3333   | IT      | 1         |
      | 381      | 5           | 5       | 1        | 3333   | IT      | 1         |
      | 381      | 5           | 5       | 0        | 3333   | IT      | 0         |

  Scenario: Checking active members
    When requested the member information with userId 4321 and website ES
    Then the response to the member request is:
      | memberId | website     |
      | 123      | ES          |

  Scenario: Checking non active members
    When requested the member information with userId 1234 and website ES
    Then the response to the member request is void

  Scenario: Checking activated when also expired are present
    When requested the member information with userId 1999 and website ES
    Then the response to the member request is:
      | memberId | website     |
      | 678      | ES          |

  Scenario: Checking activated when also expired are present in a different account
    When requested the member information with userId 3333 and website IT
    Then the response to the member request is:
      | memberId | website     |
      | 381      | IT          |

  Scenario: Checking attributes returned as expected
    When requested the member information with userId 7654 and website ES
    Then the response attributes include values:
      | memberId | sourceType   | monthsDuration |
      | 765      | POST_BOOKING | 6              |
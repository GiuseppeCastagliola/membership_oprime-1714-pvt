Feature: Test reactivateMembership service

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 777             | 7777   | JOHN      | WICK      |
      | 789             | 1234   | JON       | DOE       |
    And the next membership stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 321      | ES          | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |
      | 555      | ES          | DEACTIVATED         | ENABLED     | 777             | 2017-07-06     | 2018-07-06     |
      | 666      | ES          | DEACTIVATED         | DISABLED    | 789             | 2017-07-06     | 2018-07-06     |

    Scenario: Reactivate active member
      When requested to reactivate member with memberId 123
      Then the reactivation of active member with user_id 4321 in website ES and status ACTIVATED is not done

    Scenario: Reactivate pending to activate member
      When requested to reactivate member with memberId 321
      Then the pending to activate member with user_id 1234 in website ES and status PENDING_TO_ACTIVATE is not done

    Scenario Outline: Reactivate deactivated member
      Given the user-api has the following user info
        | userId   | status       | email   | brand | token                    | tokenType        |
        | <userId> | <userStatus> | <email> | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
      When requested to reactivate member with memberId <memberId>
      Then the deactived member with user_id <userId> in website ES is reactivated
      And member with memberId <memberId> has the auto renewal with status ENABLED
      And membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to <email>
      And the message with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> is sent to kafka
      Examples:
        | userId  | email                        | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  | memberId |
        | 7777    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA | 555      |
        | 7777    | neji.hyuga@edreamsodigeo.com | ACTIVE        | passwordTokenB         | false             | null           | 555      |
        | 7777    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | null                   | false             | null           | 555      |
        | 1234    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA | 666      |
        | 1234    | neji.hyuga@edreamsodigeo.com | ACTIVE        | passwordTokenB         | false             | null           | 666      |
        | 1234    | neji.hyuga@edreamsodigeo.com | PENDING_LOGIN | null                   | false             | null           | 666      |
